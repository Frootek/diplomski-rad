const {Client} = require ("../../node_modules/pg")

const pg_string_types = ['character varying','text']
const string_fill_types = {
    city : {
        label: 'City',
        icon :'bi bi-building'
    },
    color : {
        label: 'Color',
        icon :'bi bi-palette'
    },
    currency : {
        label: 'Currency',
        icon :'bi bi-cash-coin'
    },
    first_Name : {
        label: 'First Name',
        icon :'bi bi-person'
    },
    last_name : {
        label: 'Last Name',
        icon :'bi bi-person'
    },
    language : {
        label: 'Language',
        icon :'bi bi-translate'
    },
    random : {
        label: 'Random',
        icon :'bi bi-shuffle'
    },
    regex : {
        label: 'Regex',
        icon :'bi bi-braces-asterisk'
    },
    constant : {
        label: 'Constant',
        icon :'bi bi bi-type'
    },
}

const pg_date_types = ['date']

const pg_datetime_types = ['timestamp without time zone','timestamp']

const pg_number_types = ['numeric','smallint','integer', 'bigint']
const number_generator_types = ['Normal', 'Uniform', 'Exponential','Sequential']




const user  = 'postgres'
const host  = 'localhost'
/* const dB  = 'streamService' */
const dB  = 'Library'
const password  = 'Marinx98'
const port  = 5432


const {Pool}  = require("pg/lib")


const pool = new Pool({
    user: user,
    host: host,
    database: dB,
    password: password,
    port: port
});

async function func(event){
    //get table names
    const queryTableNamesLike = {
        name : "tables",
        text : `SELECT table_name
        FROM information_schema.tables
       WHERE table_schema='public'
         AND table_type='BASE TABLE';`
    }

    let tableColumns = {}

    let result = await pool.query(queryTableNamesLike).then(res => res.rows);
    

    let primary_keys_in_column = {}
    let foreign_keys_in_table = {}
    
    //get table columns
    let result2;
    for (const table in result) {
        let primary_key_list = []
        let columns = {
            text : `SELECT column_name, data_type, character_maximum_length, is_nullable 
            FROM information_schema.columns
           WHERE table_name = '${result[table].table_name}';`
        }
        result2 = await pool.query(columns).then(res => res.rows);
        tableColumns[result[table].table_name] = result2

        //get primary keys

        let table_name = result[table].table_name
        let primary_keys = {
            text : `SELECT c.column_name
            FROM information_schema.table_constraints tc 
            JOIN information_schema.constraint_column_usage AS ccu USING (constraint_schema, constraint_name) 
            JOIN information_schema.columns AS c ON c.table_schema = tc.constraint_schema
              AND tc.table_name = c.table_name AND ccu.column_name = c.column_name
            WHERE constraint_type = 'PRIMARY KEY' and tc.table_name = '${table_name}'`
        }
        result2 = await pool.query(primary_keys).then(res => res.rows);
    
        for (const key in result2) {
            if (Object.hasOwnProperty.call(result2, key)) {
                primary_key_list.push(result2[key].column_name)
            }
        }
        primary_keys_in_column[table_name] = primary_key_list

        let foreign_keys = {
            text : `
                    SELECT
                tc.table_schema, 
                tc.constraint_name, 
                tc.table_name, 
                kcu.column_name, 
                ccu.table_schema AS foreign_table_schema,
                ccu.table_name AS foreign_table_name,
                ccu.column_name AS foreign_column_name 
            FROM 
                information_schema.table_constraints AS tc 
                JOIN information_schema.key_column_usage AS kcu
                ON tc.constraint_name = kcu.constraint_name
                AND tc.table_schema = kcu.table_schema
                JOIN information_schema.constraint_column_usage AS ccu
                ON ccu.constraint_name = tc.constraint_name
                AND ccu.table_schema = tc.table_schema
            WHERE tc.constraint_type = 'FOREIGN KEY' AND  tc.table_name = '${result[table].table_name}' ;`
        }
        result3 = await pool.query(foreign_keys).then(res => res.rows);

        fks = result3
        

        let table_dependencies = []
        let foreign_keys_dict = {}
        let foreign_keys_list = []


        //console.log(fks[0]);
        for(let index = 0; index < fks.length; index++) {
            foreign_keys_dict[fks[index]['column_name']] = {
                'table' : fks[index]['foreign_table_name'],
                'column' : fks[index]['foreign_column_name']
            }
            foreign_keys_list.push(fks[index]['column_name'])
            table_dependencies.push(fks[index]['foreign_table_name'])
        }
        
        foreign_keys_in_table[result[table].table_name] = {
            'table_dependencies' : table_dependencies,
            'foreign_keys_dict' : foreign_keys_dict,
            'foreign_keys_list' : foreign_keys_list
        }        
    }

    var construct = {}
    
    for (const key in tableColumns) {
        if (Object.hasOwnProperty.call(tableColumns, key)) {
            const element = tableColumns[key];
            //console.log(element)
            //console.log(key)
            construct[key] = {}
            construct[key]['table_name'] = key
            construct[key]['selected'] = false
            construct[key]['collapsed'] = true
            construct[key]['number_of_rows'] = 100
            construct[key]['data_generated'] = false
            construct[key]['visited'] = false
            construct[key]['columns'] = {}

/*             console.log(key + 'ovo je undefined' + foreign_keys_in_table[key])
            console.log('ovo je foreign keys in table ' + foreign_keys_in_table ) */
            construct[key]['foreign_keys_in_table'] = foreign_keys_in_table[key]
            
            var j = 0
            for (const column in element) {
                construct[key]['columns'][element[column].column_name] = {}
                construct[key]['columns'][element[column].column_name]['column_name'] = element[column].column_name
                construct[key]['columns'][element[column].column_name]['data_type'] = element[column].data_type
                construct[key]['columns'][element[column].column_name]['character_maximum_length'] = element[column].character_maximum_length
                construct[key]['columns'][element[column].column_name]['is_nullable'] = element[column].is_nullable
                construct[key]['columns'][element[column].column_name]['is_primary_key'] = false
                construct[key]['columns'][element[column].column_name]['is_foreign_key'] = false
                //check if primary key
                if(primary_keys_in_column[key].includes(element[column].column_name)) {
                    construct[key]['columns'][element[column].column_name]['is_primary_key'] = true
                }
                //check if foreign key
                if(foreign_keys_in_table[key]!= null && foreign_keys_in_table[key]['foreign_keys_list'].includes(element[column].column_name)) {
                    construct[key]['columns'][element[column].column_name]['is_foreign_key'] = true
                }
                //maybe set default generator_settings here
                construct[key]['columns'][element[column].column_name]['generator_settings'] = 
                    getDefaultGeneratorSettings(element[column].column_name, element[column].data_type, 
                         construct[key]['columns'][element[column].column_name]['is_primary_key'],
                         construct[key]['columns'][element[column].column_name]['character_maximum_length'])
                
                ++j
            }
        }

    }
    require('util').inspect.defaultOptions.depth = null
    //console.log(JSON.stringify(construct))
    console.log(construct)

    return construct
}
let k = func().then((result) =>{console.log(result)})

//beefy function
function getDefaultGeneratorSettings(table_name, data_type, is_primary_key, character_maximum_length) {
    generator_settings = {}
    //string data type
    if(pg_string_types.includes(data_type)){
        generator_settings['type'] = 'String';
         if(table_name.match(/firstname/gi) != null){
            generator_settings['generator'] = 'First Name'
            generator_settings['null_fill_factor'] = 0
        }
        else if(table_name.match(/lastname/gi) != null){
            generator_settings['generator'] = 'Last Name'
            generator_settings['null_fill_factor'] = 0
        }
        else if(table_name.match(/city/gi) != null){
            generator_settings['generator'] = 'City'
            generator_settings['null_fill_factor'] = 0
        }
        else if(table_name.match(/color/gi) != null){
            generator_settings['generator'] = 'Color'
            generator_settings['null_fill_factor'] = 0
        }
        else if(table_name.match(/currency/gi) != null){
            generator_settings['generator'] = 'Currency'
            generator_settings['null_fill_factor'] = 0
        }
        else if(table_name.match(/language/gi) != null){
            generator_settings['generator'] = 'Language'
            generator_settings['null_fill_factor'] = 0
        }
        else {
            generator_settings['generator'] = 'Random'
            if(character_maximum_length != null) {
                generator_settings['string_length'] = character_maximum_length
            } else {
                generator_settings['string_length'] = 5
            }
            generator_settings['null_fill_factor'] = 0      
        }
    }
    //number data type
    if(pg_number_types.includes(data_type)){ 
        generator_settings['type'] = 'Number';
        if(is_primary_key) {
            generator_settings['generator'] = 'Sequential'
            generator_settings['null_fill_factor'] = 0
        }
        else {
            generator_settings['generator'] = 'Uniform'
            generator_settings['null_fill_factor'] = 0
            generator_settings['start_range'] = 0
            generator_settings['end_range'] = 100
        }
    }
    //date
    if(pg_date_types.includes(data_type)){
        generator_settings['type'] = 'Date';
        generator_settings['date_from'] = '2000-01-01'
        generator_settings['date_to'] = '2005-01-01'
        generator_settings['valid_dates'] = true
        generator_settings['null_fill_factor'] = 0
    }
    //datetime
    if(pg_datetime_types.includes(data_type)){
        generator_settings['type'] = 'Datetime'; 
        generator_settings['datetime_from'] = '2000-01-01T22:00'
        generator_settings['datetime_to'] = '2005-05-01T22:00'
        generator_settings['valid_dates'] = true
        generator_settings['null_fill_factor'] = 0
    }
    return generator_settings;

}