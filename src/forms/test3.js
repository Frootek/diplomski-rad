data = {
    owner: {
      table_name: 'owner',
      selected: false,
      collapsed: true,
      columns: {
        ownerid: {
          column_name: 'ownerid',    
          data_type: 'integer',      
          is_primary_key: true,      
          generator_settings: {}     
        },
        dateofbirth: {
          column_name: 'dateofbirth',
          data_type: 'date',
          is_primary_key: false,     
          generator_settings: {}     
        },
        oemail: {
          column_name: 'oemail',
          data_type: 'character varying',
          is_primary_key: false,
          generator_settings: {}
        },
        firstname: {
          column_name: 'firstname',
          data_type: 'character varying',
          is_primary_key: false,
          generator_settings: {}
        },
        lastname: {
          column_name: 'lastname',
          data_type: 'character varying',
          is_primary_key: false,
          generator_settings: {}
        },
        gender: {
          column_name: 'gender',
          data_type: 'character varying',
          is_primary_key: false,
          generator_settings: {}
        }
      }
    },
    show: {
      table_name: 'show',
      selected: false,
      collapsed: true,
      columns: {
        showid: {
          column_name: 'showid',
          data_type: 'integer',
          is_primary_key: true,
          generator_settings: {}
        },
        showrating: {
          column_name: 'showrating',
          data_type: 'numeric',
          is_primary_key: false,
          generator_settings: {}
        },
        showtitle: {
          column_name: 'showtitle',
          data_type: 'character varying',
          is_primary_key: false,
          generator_settings: {}
        }
      }
    },
    pack: {
      table_name: 'pack',
      selected: false,
      collapsed: true,
      columns: {
        packid: {
          column_name: 'packid',
          data_type: 'integer',
          is_primary_key: true,
          generator_settings: {}
        },
        pricepermonth: {
          column_name: 'pricepermonth',
          data_type: 'integer',
          is_primary_key: false,
          generator_settings: {}
        },
        streamno: {
          column_name: 'streamno',
          data_type: 'numeric',
          is_primary_key: false,
          generator_settings: {}
        },
        packname: {
          column_name: 'packname',
          data_type: 'character varying',
          is_primary_key: false,
          generator_settings: {}
        }
      }
    },
    track: {
      table_name: 'track',
      selected: false,
      collapsed: true,
      columns: {
        trackid: {
          column_name: 'trackid',
          data_type: 'integer',
          is_primary_key: true,
          generator_settings: {}
        },
        releasedate: {
          column_name: 'releasedate',
          data_type: 'date',
          is_primary_key: false,
          generator_settings: {}
        },
        duration: {
          column_name: 'duration',
          data_type: 'interval',
          is_primary_key: false,
          generator_settings: {}
        },
        tagerestriction: {
          column_name: 'tagerestriction',
          data_type: 'integer',
          is_primary_key: false,
          generator_settings: {}
        },
        trackrating: {
          column_name: 'trackrating',
          data_type: 'numeric',
          is_primary_key: false,
          generator_settings: {}
        },
        castandcrew: {
          column_name: 'castandcrew',
          data_type: 'ARRAY',
          is_primary_key: false,
          generator_settings: {}
        },
        director: {
          column_name: 'director',
          data_type: 'ARRAY',
          is_primary_key: false,
          generator_settings: {}
        },
        tracktitle: {
          column_name: 'tracktitle',
          data_type: 'character varying',
          is_primary_key: false,
          generator_settings: {}
        },
        countryoforigin: {
          column_name: 'countryoforigin',
          data_type: 'ARRAY',
          is_primary_key: false,
          generator_settings: {}
        },
        categories: {
          column_name: 'categories',
          data_type: 'ARRAY',
          is_primary_key: false,
          generator_settings: {}
        },
        description: {
          column_name: 'description',
          data_type: 'character varying',
          is_primary_key: false,
          generator_settings: {}
        }
      }
    },
    ownerpack: {
      table_name: 'ownerpack',
      selected: false,
      collapsed: true,
      columns: {
        packid: {
          column_name: 'packid',
          data_type: 'integer',
          is_primary_key: true,
          generator_settings: {}
        },
        ownerid: {
          column_name: 'ownerid',
          data_type: 'integer',
          is_primary_key: true,
          generator_settings: {}
        },
        packstartdatetime: {
          column_name: 'packstartdatetime',
          data_type: 'timestamp without time zone',
          is_primary_key: true,
          generator_settings: {}
        },
        packenddatetime: {
          column_name: 'packenddatetime',
          data_type: 'timestamp without time zone',
          is_primary_key: false,
          generator_settings: {}
        }
      }
    },
    genre: {
      table_name: 'genre',
      selected: false,
      collapsed: true,
      columns: {
        genreid: {
          column_name: 'genreid',
          data_type: 'integer',
          is_primary_key: true,
          generator_settings: {}
        },
        supgenreid: {
          column_name: 'supgenreid',
          data_type: 'integer',
          is_primary_key: false,
          generator_settings: {}
        },
        genrename: {
          column_name: 'genrename',
          data_type: 'character varying',
          is_primary_key: false,
          generator_settings: {}
        }
      }
    },
    language: {
      table_name: 'language',
      selected: false,
      collapsed: true,
      columns: {
        langid: {
          column_name: 'langid',
          data_type: 'integer',
          is_primary_key: true,
          generator_settings: {}
        },
        langname: {
          column_name: 'langname',
          data_type: 'character varying',
          is_primary_key: false,
          generator_settings: {}
        }
      }
    },
    profile: {
      table_name: 'profile',
      selected: false,
      collapsed: true,
      columns: {
        ownerid: {
          column_name: 'ownerid',
          data_type: 'integer',
          is_primary_key: true,
          generator_settings: {}
        },
        pagerestriction: {
          column_name: 'pagerestriction',
          data_type: 'integer',
          is_primary_key: false,
          generator_settings: {}
        },
        profilename: {
          column_name: 'profilename',
          data_type: 'character varying',
          is_primary_key: true,
          generator_settings: {}
        },
        pemail: {
          column_name: 'pemail',
          data_type: 'character varying',
          is_primary_key: false,
          generator_settings: {}
        }
      }
    },
    movie: {
      table_name: 'movie',
      selected: false,
      collapsed: true,
      columns: {
        trackid: {
          column_name: 'trackid',
          data_type: 'integer',
          is_primary_key: true,
          generator_settings: {}
        },
        boxincome: {
          column_name: 'boxincome',
          data_type: 'integer',
          is_primary_key: false,
          generator_settings: {}
        },
        prevmovieid: {
          column_name: 'prevmovieid',
          data_type: 'integer',
          is_primary_key: false,
          generator_settings: {}
        }
      }
    },
    showep: {
      table_name: 'showep',
      selected: false,
      collapsed: true,
      columns: {
        trackid: {
          column_name: 'trackid',
          data_type: 'integer',
          is_primary_key: true,
          generator_settings: {}
        },
        showid: {
          column_name: 'showid',
          data_type: 'integer',
          is_primary_key: false,
          generator_settings: {}
        },
        seasonno: {
          column_name: 'seasonno',
          data_type: 'integer',
          is_primary_key: false,
          generator_settings: {}
        },
        episodeno: {
          column_name: 'episodeno',
          data_type: 'integer',
          is_primary_key: false,
          generator_settings: {}
        }
      }
    },
    audiolang: {
      table_name: 'audiolang',
      selected: false,
      collapsed: true,
      columns: {
        trackid: {
          column_name: 'trackid',
          data_type: 'integer',
          is_primary_key: true,
          generator_settings: {}
        },
        audiolangid: {
          column_name: 'audiolangid',
          data_type: 'integer',
          is_primary_key: true,
          generator_settings: {}
        },
        isnative: {
          column_name: 'isnative',
          data_type: 'smallint',
          is_primary_key: false,
          generator_settings: {}
        },
        characterArrayTest: {
          column_name: 'characterArrayTest',
          data_type: 'ARRAY',
          is_primary_key: false,
          generator_settings: {}
        },
        testText: {
          column_name: 'testText',
          data_type: 'text',
          is_primary_key: false,
          generator_settings: {}
        },
        test: {
          column_name: 'test',
          data_type: 'character',
          is_primary_key: false,
          generator_settings: {}
        }
      }
    },
    profiletrack: {
      table_name: 'profiletrack',
      selected: false,
      collapsed: true,
      columns: {
        ownerid: {
          column_name: 'ownerid',
          data_type: 'integer',
          is_primary_key: true,
          generator_settings: {}
        },
        trackid: {
          column_name: 'trackid',
          data_type: 'integer',
          is_primary_key: true,
          generator_settings: {}
        },
        liked: {
          column_name: 'liked',
          data_type: 'smallint',
          is_primary_key: false,
          generator_settings: {}
        },
        profilename: {
          column_name: 'profilename',
          data_type: 'character varying',
          is_primary_key: true,
          generator_settings: {}
        }
      }
    },
    subtitle: {
      table_name: 'subtitle',
      selected: false,
      collapsed: true,
      columns: {
        trackid: {
          column_name: 'trackid',
          data_type: 'integer',
          is_primary_key: true,
          generator_settings: {}
        },
        subtitlelangid: {
          column_name: 'subtitlelangid',
          data_type: 'integer',
          is_primary_key: true,
          generator_settings: {}
        }
      }
    },
    trackgenre: {
      table_name: 'trackgenre',
      selected: false,
      collapsed: true,
      columns: {
        trackid: {
          column_name: 'trackid',
          data_type: 'integer',
          is_primary_key: true,
          generator_settings: {}
        },
        genreid: {
          column_name: 'genreid',
          data_type: 'integer',
          is_primary_key: true,
          generator_settings: {}
        }
      }
    },
    trackview: {
      table_name: 'trackview',
      selected: false,
      collapsed: true,
      columns: {
        ownerid: {
          column_name: 'ownerid',
          data_type: 'integer',
          is_primary_key: false,
          generator_settings: {}
        },
        viewstartdatetime: {
          column_name: 'viewstartdatetime',
          data_type: 'timestamp without time zone',
          is_primary_key: true,
          generator_settings: {}
        },
        trackid: {
          column_name: 'trackid',
          data_type: 'integer',
          is_primary_key: false,
          generator_settings: {}
        },
        savedprogress: {
          column_name: 'savedprogress',
          data_type: 'interval',
          is_primary_key: false,
          generator_settings: {}
        },
        viewenddatetime: {
          column_name: 'viewenddatetime',
          data_type: 'timestamp without time zone',
          is_primary_key: false,
          generator_settings: {}
        },
        deviceid: {
          column_name: 'deviceid',
          data_type: 'character varying',
          is_primary_key: true,
          generator_settings: {}
        },
        profilename: {
          column_name: 'profilename',
          data_type: 'character varying',
          is_primary_key: false,
          generator_settings: {}
        }
      }
    }
}

let selected_table_name = 'owner'

let selected_table_columns  = data[selected_table_name]['columns'];

console.log(Object.keys(selected_table_columns));

