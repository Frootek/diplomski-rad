const {Client} = require ("../../node_modules/pg")

const user  = 'postgres'
const host  = 'localhost'
const dB  = 'Library'
const password  = 'Marinx98'
const port  = 5432


const {Pool}  = require("pg/lib")



const pool = new Pool({
    user: user,
    host: host,
    database: dB,
    password: password,
    port: port
});

async function func(event){

    const queryTableNamesLike = {
        name : "tables",
        text : `SELECT table_name
        FROM information_schema.tables
       WHERE table_schema='public'
         AND table_type='BASE TABLE';`
    }

    let tableColumns = {}

    let result = await pool.query(queryTableNamesLike).then(res => res.rows);

    //console.log('tu sam' + JSON.stringify(result));
    let result2;
    for (const table in result) {
        let foreign_keys = {
            text : `
                    SELECT
                tc.table_schema, 
                tc.constraint_name, 
                tc.table_name, 
                kcu.column_name, 
                ccu.table_schema AS foreign_table_schema,
                ccu.table_name AS foreign_table_name,
                ccu.column_name AS foreign_column_name 
            FROM 
                information_schema.table_constraints AS tc 
                JOIN information_schema.key_column_usage AS kcu
                ON tc.constraint_name = kcu.constraint_name
                AND tc.table_schema = kcu.table_schema
                JOIN information_schema.constraint_column_usage AS ccu
                ON ccu.constraint_name = tc.constraint_name
                AND ccu.table_schema = tc.table_schema
            WHERE tc.constraint_type = 'FOREIGN KEY' AND  tc.table_name = '${result[table].table_name}' ;`
        }
        result2 = await pool.query(foreign_keys).then(res => res.rows);

        if(result[table].table_name=='Book'){
            fks = result2
            console.log('ovo je tablica' + result[table].table_name)

            let table_dependencies = []
            let foreign_keys = {}
            let foreign_keys_list = []

            console.log(fks[0]);
            for(let index = 0; index < fks.length; index++) {
                foreign_keys[fks[index]['column_name']] = {
                    'table' : fks[index]['foreign_table_name'],
                    'column' : fks[index]['foreign_column_name']
                }
                foreign_keys_list.push(fks[index]['column_name'])
                table_dependencies.push(fks[index]['foreign_table_name'])
            }
            
            console.log(`Foreign keys: ` + JSON.stringify(foreign_keys))
            console.log(`Table dependencies: ` + table_dependencies)
        }
    }
  
}
func()