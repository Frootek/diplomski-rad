export const pg_string_types = ['character varying','text']
export const string_fill_types = {
    city : {
        label: 'City',
        icon :'bi bi-building'
    },
    city_cro : {
        label: 'City (CRO)',
        icon :'bi bi-building'
    },
    color : {
        label: 'Color',
        icon :'bi bi-palette'
    },
    currency : {
        label: 'Currency',
        icon :'bi bi-cash-coin'
    },
    first_name : {
        label: 'First Name',
        icon :'bi bi-person'
    },
    first_name_cro : {
        label: 'First Name (CRO)',
        icon :'bi bi-person'
    },
    last_name : {
        label: 'Last Name',
        icon :'bi bi-person'
    },
    last_name_cro : {
        label: 'Last Name (CRO)',
        icon :'bi bi-person'
    },
    language : {
        label: 'Language',
        icon :'bi bi-translate'
    },
    random : {
        label: 'Random',
        icon :'bi bi-shuffle'
    },
    regex : {
        label: 'Regex',
        icon :'bi bi-braces-asterisk'
    },
    constant : {
        label: 'Constants',
        icon :'bi bi bi-type'
    },
}

export const pg_date_types = ['date']

export const pg_datetime_types = ['timestamp without time zone','timestamp']

export const pg_number_types = ['numeric','smallint','integer', 'bigint']
export const number_distribution_types = ['Normal', 'Random', 'Uniform','Sequential']


//za regex ako budes podrzavao ikad
{/* <div v-if="selected_fill_type=='Regex'" class="row" style="margin-top:20px">
<div class="col-sm-6">
    <label style="margin-top:5px">Enter valid regex:</label>
</div>

<div class="col-sm-6">
<input v-model="valid_constant_string" type="string" class="form-control">
</div>
<div v-if="!valid_regex_pattern" class="invalid-feedback">
    Regular expression is invalid.
</div>
</div>    */}