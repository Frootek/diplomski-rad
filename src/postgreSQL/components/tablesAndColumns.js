import  * as helper from './helper.js'
var path = require("path")
const db = require(path.join(__dirname,"/db/index.js"))

const ipcRenderer = require('electron').ipcRenderer;
const dialog  = require('electron').remote.dialog

const fs = require('fs');

export default {

props : {
  tablesAndColumns : Object,
},
data() {
  return {
    checkedTables : [],
    all_collapsed : true,
    all_selected : false,
    selected_row_id : null,
    search_input : '',
  }
},
emits: [ 'noSelectedRow', 'selectedTableRow', 'selectedColumnRow'],
watch: {
  selected_row_id (new_value) {
    if(new_value !== null) {
      if(new_value.split('-')[0] == 'tableRow') {

        this.$emit('selectedTableRow', new_value.split('-')[1])
      } 
      else if(new_value.split('-')[0] == 'columnRow') {
        this.$emit('selectedColumnRow', new_value.split('-')[1], new_value.split('-')[2])
      }

    }
    else  {
      this.$emit('noSelectedRow')
    }

  }
},
methods : {
  toggleTableColumnsSection(event) {
    event.currentTarget.blur()
    var buttonId = event.currentTarget.id
    let table_name = buttonId.split("-")[1]
    this.tablesAndColumns[table_name]['collapsed'] = !this.tablesAndColumns[table_name]['collapsed']
  },
  
  toggleAllCollapse(event) {
    event.currentTarget.blur()
    this.all_collapsed = !this.all_collapsed;
    for (const key in this.tablesAndColumns) {
      if (Object.hasOwnProperty.call(this.tablesAndColumns, key)) {
        this.tablesAndColumns[key]['collapsed'] = this.all_collapsed;
      }
    }
  },
  toggleSelectAll(event) {
      this.all_selected = !this.all_selected
      for (const key in this.tablesAndColumns) {
        if (Object.hasOwnProperty.call(this.tablesAndColumns, key)) {
          this.tablesAndColumns[key]['selected'] = this.all_selected;
          if(this.all_selected){
            this.checkedTables.push(key)
          }
        }
      }
      if(!this.all_selected){
        this.checkedTables = []
      }
  },
  tableRowClicked(event) {
    let clicked_table_row_id = event.currentTarget.id
    let id_parts = clicked_table_row_id.split("-")
    let table_name = id_parts[1];

    if(this.selected_row_id == clicked_table_row_id) {
      this.selected_row_id = null
    }
    else {
      this.selected_row_id = clicked_table_row_id
    }
  },
  columnRowClicked(event) {
    //logic of getting table name and column name from row id
    let clicked_column_row_id = event.currentTarget.id
    let id_parts = clicked_column_row_id.split("-")
    let table_name = id_parts[1];
    let column_name = id_parts[2];

    if(this.selected_row_id == clicked_column_row_id) {
      this.selected_row_id = null
    }
    else {
      this.selected_row_id = clicked_column_row_id
    }
  },
  filter(table_name) {
    if(this.search_input == null || this.search_input == '') {
      return true
    }
    if(table_name.includes(this.search_input)) {
      return true
    }
    return false;    
  },

  async deepResolve(tableName, tableOrder) {
    let dependencies = this.tablesAndColumns[tableName]['foreign_keys_in_table']['table_dependencies']
    
    for (let index = 0; index < dependencies.length; index++) {
      const element = dependencies[index];
      if(!tableOrder.includes(element)) {
        this.deepResolve(tableName, tableOrder)
      }
      
    }
    tableOrder.push(tableName)

  },  
  async orderOfGeneration() {
    let tableOrder = []


    //first add all tables that have no dependency
    for (let i = 0; i < this.checkedTables.length; i++) {
      let element = this.checkedTables[i];

      if(this.tablesAndColumns[element]['foreign_keys_in_table']['table_dependencies'].length == 0){
        this.tablesAndColumns[element]['visited'] = true
        tableOrder.push(element)
      }
    }

    for (let i = 0; i < this.checkedTables.length; i++) {
      let element = this.checkedTables[i];

      if(tableOrder.includes(element)) {
        continue;
      }

      // lets resolve tables with dependencies

      this.deepResolve(element, tableOrder)

    }
      return tableOrder
  },
  async generateData(event){

    const notification = {
      title: 'Data loading has started.',
      body: 'Your data will be imported shortly...'  ,
      icon: path.join(__dirname, '../../assets/images/postgres_logo.png')
    }
    new window.Notification(notification.title,notification)
    
    console.log('This is generate data');
    console.log('This is checked tables');
    //this table we will generate data
    console.log(this.tablesAndColumns[this.checkedTables[0]]);

    let tableOrder = await this.orderOfGeneration();
    console.log(tableOrder);
    

    for (let index = 0; index < tableOrder.length; index++) {
      let table = this.tablesAndColumns[tableOrder[index]]
      //we need to create
      let column_datas = {}

      //build
      let insert_prefix = "INSERT INTO "  + tableOrder[index] + "("
      //"INSERT INTO owner(ownerid, dateofbirth, oemail, firstname, lastname, gender) VALUES('1', '2013-10-19', 'gAU1Q', 'Peter', 'Wood', 'm')"

      for (const [key, value] of Object.entries(table['columns'])) {
        console.log(`${key}: ${JSON.stringify(value)}`);
        column_datas[key] = 
          helper.generateDataForColumn(value['generator_settings'], this.tablesAndColumns[tableOrder[index]]['number_of_rows'] ,tableOrder[index],key ,this.tablesAndColumns)
        insert_prefix += key + ', '
      }
      console.log('ovo su ti column datas' + JSON.stringify(column_datas))
      insert_prefix = insert_prefix.slice(0,-2)
      insert_prefix += ")"
      //100 is number of rows
      for (let j = 0; j < table['number_of_rows']; j++) {
        let insert_suffix = " VALUES("
        for (const [key, value] of Object.entries(table['columns'])) {
          insert_suffix += "'" + column_datas[key][j] + "', " 

        }
        insert_suffix = insert_suffix.slice(0, -2)
        insert_suffix += ')'

        let query = {
          text: insert_prefix + insert_suffix
        }
        
        let result2 = await db.query(query).then(res => res.rows).catch(e => console.log(e));
      }      
    }
  },
  exportConfig() {
    ipcRenderer.send('select-directory',this.tablesAndColumns)

    //console.log('this is export', )
  },
  async importConfig() {
    dialog.showOpenDialog({
      properties: ['openFile'] 
    }).then(result => {
      try {

        let config_obj = JSON.parse(fs.readFileSync(result['filePaths'][0], 'utf8'));
        
        //loop through columns and update it to config
        for (const key in this.tablesAndColumns) {
          if (Object.hasOwnProperty.call(this.tablesAndColumns, key)) {
            if(config_obj.hasOwnProperty(key)){
              this.tablesAndColumns[key] = config_obj[key];
            }
          }
        }
        ipcRenderer.send('file-imported' , result['filePaths'][0])
      }
      catch(e) { 
        console.log('error==>'+ e) 
      }
    });

  },
},
  template: `
  <div class="container-fluid" style="height:100%">

    <div class="container-fluid">
      
        <div class="row">
          <div class="col-xs-3">
            <h3 style="padding-top:10px">Tables and Columns</h3>
          </div>
          <div class="col-xs-3">
            <button style="margin-top:25px;" type="button" class="btn btn-primary" @click="exportConfig($event)">Export config</button>
            <button style="margin-top:25px; margin-left:15px" type="button" class="btn btn-primary" @click="importConfig($event)">Import config</button>
          </div>
          <div class="col-xs-1">
            <input v-model="search_input"  style="margin-top:30px" placeholder="search..." />
          </div>
          <div class="col pull-right" style="padding-right:15px">
            <button v-if="checkedTables.length != 0" style="margin-top:25px" type="button" class="btn btn-success" @click="generateData($event)">Generate data</button>
            <button v-else style="margin-top:25px" type="button" class="btn btn-secondary" @click="generateData($event)">Generate data</button>
          </div>     
        </div>
          
        <hr style="border-width:1.5px ;color:black;">
    </div>

    <div class="container-fluid" style="height:70%">

      <div class="container-fluid">
        <div class="row">
          
          <div class="col-xs-1" style="padding-left:0px">
            <button type="button" @click="toggleAllCollapse($event)" class="btn btn-dark btn-xs">
              <i v-if="all_collapsed" class="bi bi-arrows-angle-expand"></i>
              <i v-else class="bi bi-arrows-angle-contract"></i>
            </button>
          </div>
          
          <div class="col-xs-1" style="">
            <input type="checkbox" @click="toggleSelectAll($event)" id="checkAllTables"> 
          </div>
          
          <div class="col-xs-4" style="padding-left:5px;">Name</div>
          <div class="col-xs-3" style="">Data Type</div>
          <div class="col-xs-3" style="">Fill type</div>
        </div>
      </div>

      <div class="container-fluid tablesAndColumnsRows" style="height:90%; margin-top:5px">
        <div v-for="(table,tables) in tablesAndColumns">

          <div  v-if="filter(table.table_name)" :class="'tableRow-'+ table.table_name === selected_row_id ? 'row table-row column-row-clicked' :' row table-row '" :id="'tableRow-'+ table.table_name" @click="tableRowClicked($event)" style="margin-top:5px">
            <div class="col-xs-1" style="padding-left:0px">
              <button type="button" :id="'collapseBtn-'+ table.table_name" @click="toggleTableColumnsSection"  @click.stop="" class="btn btn-dark btn-xs">
                <i v-if="table.collapsed" :id="table.table_name + '_collapse_icon'" class="bi bi-plus"></i>
                <i v-else-if="!table.collapsed" :id="table.table_name + '_collapse_icon'" class="bi bi-dash"></i>
              </button>
            </div>

            <div class="col-xs-1" style="">
              <input @click="table.selected = !table.selected"  @click.stop="" type="checkbox" :id="'checkbox-'+ table.table_name" :value="table.table_name" v-model="checkedTables" :checked="table.selected"  />
            </div>
            <div class="col-xs-4" style="padding-left:5px;"><b>{{table.table_name}}</b></div>
            <div class="col-xs-3" style=""></div>
            <div class="col-xs-3" style=""><b>{{table.number_of_rows}} rows</b></div>
          </div>
          
          <div  v-if="!table.collapsed && filter(table.table_name)" v-for="(column,columns) in table.columns">
            <div  :class="'columnRow-'+ table.table_name +'-'+ column.column_name === selected_row_id ? 'row column-row column-row-clicked' :' row column-row '" :id="'columnRow-'+ table.table_name +'-'+ column.column_name" @click="columnRowClicked($event)" style="padding-top:5px">
              <div class="col-xs-1" style=""></div>
              <div class="col-xs-1" style=""></div>
              <div class="col-xs-4" style="padding-left:10px;">
                <i v-if="column.is_primary_key || column.is_foreign_key" class="bi bi-key"></i>
                <span v-else-if="column.data_type == 'character varying' || column.data_type == 'text'" ><i class="bi bi-file-text"></i></span>
                <span v-else-if="column.data_type.includes('time') ||  column.data_type.includes('date') " ><i class="bi bi-calendar-week"></i></span>
                <span v-else-if="column.data_type.includes('int') ||column.data_type.includes('numeric')"><i class="bi bi-hash"></i></span>
                <span v-else-if="column.data_type.toLowerCase().includes('array')"><i class="bi bi-braces"></i></span>
                {{column.column_name}}
              </div>
              <div v-if="column.data_type == 'character varying' || column.data_type == 'text'" class="col-xs-3" >{{column.data_type}}({{column.character_maximum_length}})</div>
              <div v-else class="col-xs-3" style=""> 
                <span v-if="column.is_primary_key" >PK - {{column.data_type}} </span><span v-else-if="column.is_foreign_key" >FK - {{tablesAndColumns[table.table_name]['foreign_keys_in_table']['foreign_keys_dict'][column.column_name]['table']}} ({{tablesAndColumns[table.table_name]['foreign_keys_in_table']['foreign_keys_dict'][column.column_name]['column']}}) </span>
                <span v-else>{{column.data_type}}</span>
              </div>
              <div class="col-xs-3" style="">{{column.generator_settings.type}} - {{column.generator_settings.generator}}</div>
            </div>        

          </div>
        </div>
      </div> 
    </div>
  </div>
  `      
}
