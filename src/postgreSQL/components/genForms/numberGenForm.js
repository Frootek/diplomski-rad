import { number_distribution_types } from "../constants.js"

import { normalDistribution } from "./normalDistribution.js"

import { uniformDistribution } from "./uniformDistribution.js"

export default {

    props: {
        generatorSettings : Object,
        numberOfRows : Number,
        isPrimaryKey: Boolean,
        isForeignKey: Boolean,
        isNullable : String,
        key : Number
    },
    emits: [ 'updateColumnGenSettings'],
    data() {
        return {
            form : null,
            null_fill_factor : null,
            start_range : null,
            start_range_disabled: false,
            end_range : null,
            end_range_disabled : false,
            valid_range : true,
            generator : null,
            numberDistributionTypes : number_distribution_types
        }
    },
    watch : {
        null_fill_factor(newValue) {
            if(newValue>=100) {
                this.null_fill_factor = 100
            }
            if(newValue<0) {
                this.null_fill_factor = 0
            }
        },
        start_range(newValue) {
            if(this.generator == 'Sequential') {
                this.end_range = this.start_range + this.numberOfRows
            }

            if(newValue > this.end_range) {
                this.start_range = this.end_range - 1
            }
            this.updateDistributionGraph()
        },
        end_range(newValue) {
            if(newValue < this.start_range) {
                this.end_range = this.start_range + 1
            }
            this.updateDistributionGraph()
        },
        generator(newValue) {
            if(newValue == 'Sequential') {
                this.end_range = this.start_range + this.numberOfRows
                this.end_range_disabled = true
            }
            else {
                this.end_range_disabled = false
            }
            this.updateDistributionGraph()
        }

    },
    methods : {
        isRangeValid() {
            if(start_range <= end_range) {
                return true
            }
            else {
                return false
            }
        },
    },
    mounted() {
        this.null_fill_factor = this.generatorSettings['null_fill_factor'] == null ? '0' : this.generatorSettings['null_fill_factor']
        this.start_range = this.generatorSettings['start_range'] == null ? 0: this.generatorSettings['start_range']  
        this.end_range = this.generatorSettings['end_range'] == null ? 100 : this.generatorSettings['end_range']
        
        if(this.isPrimaryKey || this.isForeignKey) {
            this.end_range = this.numberOfRows
        }
        
        this.generator = this.generatorSettings['generator'] == null ? 'Random' : this.generatorSettings['generator']  
        //this.generator = 'Random'
    },
    methods : {
        updateGenSettings() {
            let newGenSettings = {
                'type' : 'Number',
                'generator' : this.generator,
                'start_range' : this.start_range,
                'end_range' : this.end_range,
                'null_fill_factor' : this.null_fill_factor,
            }

            this.$emit('updateColumnGenSettings', newGenSettings)
        },
        updateDistributionGraph(){
            if(this.valid_range) {
                // Initialize the echarts instance based on the prepared dom
                var myChart = echarts.init(document.getElementById('numberDistribution'));
                let data = {}
                let data_x_axis = []
                let data_y_axis = []

                if(this.generator == 'Random') {
                    for (let i = 0; i < this.numberOfRows; i++) {
                        let number = Math.floor(Math.random() * (this.end_range + 1 - this.start_range)) + this.start_range 
                        if(data[number]){
                            data[number] = data[number] + 1
                        } else {
                            data[number] = 1
                        }
                    }
            
                    for (let i = this.start_range; i <= this.end_range; i++) {
                        data_x_axis.push(i)
                        if(data[i]) {
                            data_y_axis.push(data[i])
                        } else {
                            data_y_axis.push(0)
                        }
                    }
                }

                if(this.generator == 'Normal') {
                    data = normalDistribution(this.start_range, this.end_range, this.numberOfRows)
                
                    for (const [key, value] of Object.entries(data)) {
                        data_x_axis.push(key)
                        data_y_axis.push(value)
                    }                
                }
                if(this.generator == 'Uniform') {
                    data = uniformDistribution(this.start_range, this.end_range, this.numberOfRows)
                    for (const [key, value] of Object.entries(data)) {
                        data_x_axis.push(key)
                        data_y_axis.push(value)
                    }    
                }
                if(this.generator == 'Sequential') {
                    for (let index = this.start_range; index < this.end_range; index++) {
                        data[index] = 1
                    }
                    for (const [key, value] of Object.entries(data)) {
                        data_x_axis.push(key)
                        data_y_axis.push(value)
                    }
                }
                // Specify the configuration items and data for the chart
                var option = {
                    tooltip: {},
                    legend: {
                    data: ['distribution']
                    },
                    xAxis: {
                    data: data_x_axis
                    },
                    yAxis: {},
                    series: [
                    {
                        type: 'bar',
                        data: data_y_axis
                    }
                    ]
                };
        
                // Display the chart using the configuration items and data just specified.
                myChart.setOption(option);                
            }
        } 
    },
    template: `
    <div class="container-fluid my-form">
    <div class="form-group" style="padding-right:5px">

        <div class="row">
            <div class="col-sm-6">
                <label style="margin-top:5px">Start range:</label>
            </div>

            <div class="col-sm-6">
            <input v-model="start_range" type="number" class="form-control" :disabled="this.isPrimaryKey || this.start_range_disabled ||  this.isForeignKey">
            </div>
            <div v-if="start_range > end_range" class="invalid-feedback col-sm-6">
                Start range should be <= end date.
            </div>
        </div>   

        <div class="row"  style="margin-top:20px">
            <div class="col-sm-6">
                <label style="margin-top:5px">End range:</label>
            </div>

            <div class="col-sm-6">
            <input v-model="end_range" type="number" class="form-control" :disabled="this.isPrimaryKey || this.end_range_disabled ||  this.isForeignKey" >
            </div>
            <div v-if="start_range > end_range" class="invalid-feedback col-sm-6">
                End range should be >= start range.
            </div>
        </div>   

        <div class="row" style="margin-top:20px">
            <div class="col-sm-6">
                <label style="margin-top:5px">Distribution:</label>
            </div>
            <div class="col-sm-6">
                <select v-model="generator" class="form-control" :disabled="this.isPrimaryKey ||  this.isForeignKey">
                    <option v-for="distribution in numberDistributionTypes">
                        {{distribution}}
                    </option>
                </select>
            </div>
        </div>
    
        <div class="row" style="margin-top:20px">
            <div class="col-sm-6">
                <label style="margin-top:5px">NULL fill factor (%) :</label>
            </div>

            <div v-if="isNullable =='YES'" class="col-sm-6">
            <input v-model="null_fill_factor" :disabled="this.isPrimaryKey ||  this.isForeignKey" type="number" class="form-control" min="0" max="100">
            </div>
            <div v-if="isNullable =='NO'" class="col-sm-6">
            <input v-model="null_fill_factor" disabled type="number" class="form-control" min="0" max="100">
            </div>

        </div>

        <div class="row" style="margin-top:20px">                  
            <div class="col pull-right" style="padding-right:15px">
                <button type="button" :disabled="this.isPrimaryKey ||  this.isForeignKey" class="btn btn-success" @click="updateGenSettings">Save</button>
            </div>
        </div>
    
        <div class="row" style="margin-top:10px;">
            <div class="col" style="padding-right:15px">
                <div id="numberDistribution" style="width: 550px; height:200px;"></div>
            </div>
        </div> 
        

    </div>

</div>
    `      
}