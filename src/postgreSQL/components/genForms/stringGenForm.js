import { string_fill_types } from "../constants.js"

export default {

    props: {
        generatorSettings : Object,
        isNullable : String,
        characterMaximumLength : Number,
        key : Number
    },
    emits: [ 'updateColumnGenSettings'],
    data() {
        return {
            form : null,
            stringFillTypes : string_fill_types,
            selected_fill_type : null,
            null_fill_factor : null,
            string_length : null,
            regex_pattern : null,
            valid_regex_pattern : null,
            constant_string: null,
        }
    },
    watch : {
        null_fill_factor(newValue) {
            if(newValue>=100) {
                this.null_fill_factor = 100
            }
            if(newValue<0) {
                this.null_fill_factor = 0
            }
        },
        regex_pattern(newValue) {
            if(this.characterMaximumLength != null && this.regex_pattern.length > this.characterMaximumLength) {
                this.regex_pattern  = this.regex_pattern.slice(1,this.characterMaximumLength+1)
            }
        },
        string_length(newValue) {
            if(this.characterMaximumLength != null && newValue > this.characterMaximumLength) {
                this.string_length = this.characterMaximumLength
            }
            if(newValue<=0) {
                this.string_length = 1
            }         
        },
        constant_string(newValue){
            
            if(this.characterMaximumLength == null) {
                return
            }

            let last_character = newValue.charAt(newValue.length-1)
            if(last_character == '' || last_character == ";") {
                return
            }

            let constants = newValue.split(";")
            
            let last_element = constants.pop()

            if(last_element.length > this.characterMaximumLength) {
              last_element  = last_element.slice(1,this.characterMaximumLength+1)
            }

            let constants_string
            
            if(constants.length != 0) {
                constants_string = constants.join(";") + ";" + last_element
            }else {
                constants_string = last_element
            }

            if(last_character === ";"){
                this.constant_string = constants_string + last_character
            } else {
                this.constant_string = constants_string
            }
        }
    },
    mounted() {
        this.selected_fill_type = this.generatorSettings['generator'] == null ? 'Random' : this.generatorSettings['generator'] 
        this.null_fill_factor = this.generatorSettings['null_fill_factor'] == null ? '0' : this.generatorSettings['null_fill_factor']
        
        this.string_length = this.generatorSettings['string_length'] == null ? 5 : this.generatorSettings['string_length']
        if(this.characterMaximumLength != null && this.string_length > this.characterMaximumLength) {
            this.string_length = this.characterMaximumLength
        }

        this.regex_pattern = this.generatorSettings['regex_pattern'] == null ? '' : this.generatorSettings['regex_pattern']  
        this.valid_regex_pattern = this.generatorSettings['valid_regex_pattern'] == null ? true : this.generatorSettings['valid_regex_pattern']  
        this.constant_string = this.generatorSettings['constant_string'] == null ? 'abc' : this.generatorSettings['constant_string']  
    },
    methods : {
        updateGenSettings() {
            let newGenSettings = {
                'type' : 'String',
                'generator' : this.selected_fill_type,
                'null_fill_factor' : this.null_fill_factor,
                'string_length' : this.string_length,
                'constant_string' : this.constant_string,
                'regex_pattern' : this.regex_pattern,
            }

            this.$emit('updateColumnGenSettings', newGenSettings)
        }
    },
    template: `
        <div class="container-fluid">
            <div class="form-group">

                <div class="row">
                    <div class="col-sm-6">
                        <label style="margin-top:5px">Fill type:</label>
                    </div>
                    <div class="col-sm-6">
                        <select v-model="selected_fill_type" class="form-control">
                            <option v-for="(fill, fillType) in stringFillTypes">
                                {{fill.label}}
                            </option>
                        </select>
                    </div>
                </div>

                <div v-if="selected_fill_type=='Random'" class="row" style="margin-top:20px">

                    <div class="col-sm-6">
                        <label style="margin-top:5px">Random string max length:</label>
                    </div>

                    <div class="col-sm-6">
                    <input v-model="string_length" type="number" class="form-control">
                    </div>
                </div>
            
                <div v-if="selected_fill_type=='Constants'" class="row" style="margin-top:20px">
                    <div class="col-sm-6">
                        <label style="margin-top:5px">Enter string:</label>
                    </div>
                    <div class="col-sm-6">
                        <input v-model="constant_string" type="string" class="form-control">
                    </div>
                </div>

                <div v-if="selected_fill_type=='Regex'" class="row" style="margin-top:20px">
                    <div class="col-sm-6">
                        <label style="margin-top:5px">Enter simple regex:</label>
                    </div>
                    
                    <div class="col-sm-6">
                    <input v-model="regex_pattern" type="string" class="form-control">
                    </div>
                </div> 

                <div class="row" style="margin-top:20px">
                    <div class="col-sm-6">
                        <label style="margin-top:5px">NULL fill factor (%) :</label>
                    </div>

                    <div v-if="isNullable =='YES'" class="col-sm-6">
                    <input v-model="null_fill_factor" type="number" class="form-control" min="0" max="100">
                    </div>
                    <div v-if="isNullable =='NO'" class="col-sm-6">
                    <input v-model="null_fill_factor" disabled type="number" class="form-control" min="0" max="100">
                    </div>
                </div>


                <div class="row" style="margin-top:20px">                  
                    <div class="col pull-right" style="padding-right:15px">
                        <button type="button" class="btn btn-success" @click="updateGenSettings">Save</button>
                    </div>
                </div>    

            </div>

        </div>
    `      
}