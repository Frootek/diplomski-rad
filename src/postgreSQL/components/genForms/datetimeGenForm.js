
export default {
    props: {
        generatorSettings : Object,
        isNullable : String,
        key : Number
    },
    emits: [ 'updateColumnGenSettings'],
    data() {
        return {
            form : null,
            null_fill_factor : null,
            datetime_from : null,
            datetime_to : null,
            valid_dates : true,
            distribution : null,
        }
    },
    watch : {
        null_fill_factor(newValue) {
            if(newValue>=100) {
                this.null_fill_factor = 100
            }
            if(newValue<0) {
                this.null_fill_factor = 0
            }
        },
        datetime_from(newValue) {
            console.log(newValue)
        }
    },
    methods : {
        areDatesValid() {
            if(datetime_from<=datetime_to) {
                return true
            }
            else {
                return false
            }
        },
        updateGenSettings() {
            let newGenSettings = {
                'type' : 'Datetime',
                'null_fill_factor' : this.null_fill_factor,
                'datetime_from' : this.datetime_from,
                'datetime_to' : this.datetime_to,
                'valid_dates' : true
            }
            this.$emit('updateColumnGenSettings', newGenSettings)
        }
    },
    mounted() {
        this.null_fill_factor = this.generatorSettings['null_fill_factor'] == null ? '0' : this.generatorSettings['null_fill_factor']
        this.datetime_from = this.generatorSettings['datetime_from'] == null ? '2000-01-01T22:00' : this.generatorSettings['datetime_from']  
        this.datetime_to = this.generatorSettings['datetime_to'] == null ? '2005-05-01T22:00' : this.generatorSettings['datetime_to']  
        this.valid_dates = this.generatorSettings['valid_dates'] == null ? true : this.generatorSettings['valid_dates']  
    },
    template: `
    <div class="container-fluid">
    <div class="form-group">

        <div class="row">
            <div class="col-sm-6">
                <label style="margin-top:5px">Start date range:</label>
            </div>

            <div class="col-sm-6">
            <input v-model="datetime_from" type="datetime-local" class="form-control"  >
            </div>

        </div>   

        <div class="row"  style="margin-top:20px">
            <div class="col-sm-6">
                <label style="margin-top:5px">End date range:</label>
            </div>

            <div class="col-sm-6">
            <input v-model="datetime_to" type="datetime-local" class="form-control" >
            </div>
        </div>   
        

        <div class="row" style="margin-top:20px">
            <div class="col-sm-6">
                <label style="margin-top:5px">NULL fill factor (%) :</label>
            </div>

            <div class="col-sm-6">
            <input v-model="null_fill_factor" type="number" :disabled="isNullable == 'YES'" class="form-control" min="0" max="100">
            </div>
        </div>


        <div class="row" style="margin-top:20px">                  
            <div class="col pull-right" style="padding-right:15px">
                <button type="button" class="btn btn-success" @click="updateGenSettings">Save</button>
            </div>
        </div>    

    </div>

</div>
    `      
}
