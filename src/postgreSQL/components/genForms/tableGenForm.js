
export default {
    props: {
        numberOfRows : Number,
        key : Number
    },
    emits: [ 'updateTableSettings'],
    data() {
        return {
            number_of_rows : null,
        }
    },
    watch : {
        number_of_rows(newValue) {
            if(newValue<0) {
                this.number_of_rows = 0
            }
        },
    },
    methods : {
        updateTableSettings() {
            if(this.numberOfRows < 100){
                this.numberOfRows = 100
            }
            this.$emit('updateTableSettings', this.number_of_rows)
        }
    },
    mounted() {
        this.number_of_rows = this.numberOfRows == null ? '100' : this.numberOfRows
    },
    template: `
    <div class="container-fluid">
    <div class="form-group">

        <div class="row">
            <div class="col-sm-6">
                <label style="margin-top:5px">Number of rows:</label>
            </div>

            <div class="col-sm-6">
                <input v-model="number_of_rows" type="number" class="form-control">
            </div>
        </div>  


        <div class="row" style="margin-top:20px">                  
            <div class="col pull-right" style="padding-right:15px">
                <button type="button" class="btn btn-success " @click="updateTableSettings">Save</button>
            </div>
        </div>    

    </div>

</div>
    `      
}
