let n = 10000;



const randn_bm = (min, max, skew) => {
    var u = 0, v = 0;
    while(u === 0) u = Math.random(); //Converting [0,1) to (0,1)
    while(v === 0) v = Math.random();
    let num = Math.sqrt( -2.0 * Math.log( u ) ) * Math.cos( 2.0 * Math.PI * v );

    num = num / 10.0 + 0.5; // Translate to 0 -> 1
    if (num > 1 || num < 0) num = randn_bm(min, max, skew); // resample between 0 and 1 if out of range
    num = Math.pow(num, skew); // Skew
    num *= max - min; // Stretch to fill range
    num += min; // offset to min
    return num;
}

const round_to_precision = (x, precision) => {
    var y = +x + (precision === undefined ? 0.5 : precision/2);
    return y - (y % (precision === undefined ? 1 : +precision));
}

export function normalDistribution(min, max, n) {
    let data = {};
    let step = 1;
    let skew = 1;
    // Seed data with a bunch of 0s
    for (let j=min; j<max; j+=step) {
        data[j] = 0;
    }
    
    // Create n samples between min and max
    for (i=0; i<n; i+=step) {
        let rand_num = randn_bm(min, max, 1);
        let rounded = round_to_precision(rand_num, step)
        data[rounded] += 1;
    }
    return data
}