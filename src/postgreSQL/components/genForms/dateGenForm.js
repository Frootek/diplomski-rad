
export default {

    props: {
        generatorSettings : Object,
        isNullable : String,
        key : Number
    },
    emits: [ 'updateColumnGenSettings'],
    data() {
        return {
            form : null,
            null_fill_factor : null,
            date_from : null,
            date_to : null,
            valid_dates : true,
        }
    },
    watch : {
        null_fill_factor(newValue) {
            if(newValue>=100) {
                this.null_fill_factor = 100
            }
            if(newValue<0) {
                this.null_fill_factor = 0
            }
        },
        date_from(newValue) {
            console.log(newValue)
        }
    },
    methods : {
        areDatesValid() {
            if(date_from<=date_to) {
                return true
            }
            else {
                return false
            }
        },
        updateGenSettings() {
            let newGenSettings = {
                'type' : 'Date',
                'null_fill_factor' : this.null_fill_factor,
                'date_from' : this.date_from,
                'date_to' : this.date_to,
                'valid_dates' : true
            }
            this.$emit('updateColumnGenSettings', newGenSettings)
        }
    },
    mounted() {
        this.null_fill_factor = this.generatorSettings['null_fill_factor'] == null ? '0' : this.generatorSettings['null_fill_factor']
        this.date_from = this.generatorSettings['date_from'] == null ? '2000-01-01' : this.generatorSettings['date_from']  
        this.date_to = this.generatorSettings['date_to'] == null ? '2005-01-01' : this.generatorSettings['date_to']
        this.valid_dates = this.generatorSettings['valid_dates'] == null ? true : this.generatorSettings['valid_dates']  
    },
    template: `
    <div class="container-fluid">
    <div class="form-group">

        <div class="row">
            <div class="col-sm-6">
                <label style="margin-top:5px">Start date range:</label>
            </div>

            <div class="col-sm-6">
            <input v-model="date_from" type="date" class="form-control" >
            </div>
            <div v-if="date_to<date_from" class="invalid-feedback col-sm-6">
                Start date should be <= end date.
            </div>
        </div>   

        <div class="row"  style="margin-top:20px">
            <div class="col-sm-6">
                <label style="margin-top:5px">End date range:</label>
            </div>

            <div class="col-sm-6">
            <input v-model="date_to" type="date" class="form-control" >
            </div>
            <div v-if="date_to<date_from" class="invalid-feedback col-sm-6">
                End date should be >= start date.
            </div>
        </div>   
    
        <div class="row" style="margin-top:20px">
            <div class="col-sm-6">
                <label style="margin-top:5px">NULL fill factor (%) :</label>
            </div>

            <div v-if="isNullable =='YES'" class="col-sm-6">
            <input v-model="null_fill_factor" type="number" class="form-control" min="0" max="100">
            </div>
            <div v-if="isNullable =='NO'" class="col-sm-6">
            <input v-model="null_fill_factor" disabled type="number" class="form-control" min="0" max="100">
            </div>
        </div>


        <div class="row" style="margin-top:20px">                  
            <div class="col pull-right" style="padding-right:15px">
                <button type="button" class="btn btn-success" @click="updateGenSettings">Save</button>
            </div>
        </div>    

    </div>

</div>
    `      
}
