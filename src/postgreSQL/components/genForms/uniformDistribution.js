function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}


export function uniformDistribution(min, max, n) {
    var data = {};
    var a;
    var i = 0;
    for (i = 0; i < n; ++i)
    {
        a = getRandomInt(min, max);
        if (typeof data[a] === 'undefined') { // first time initialize
        data[a] = 1;
        } else {
        data[a] = data[a] + 1;
        }
    }
    return data
}

uniformDistribution(0,100,100)