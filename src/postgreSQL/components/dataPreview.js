
export default {
    props: {
        selectedTable : String,
        tableColumns : Array,
        selectedColumn: String,
        generatedData: Object,
        numberOfRows :  Number,
    },
    data() {
        return {
            generator_settings : {}
        }
    },
    updated() {
        this.generator_settings = {}
    },
    watch : {

    },
    
    methods : {
        getCellData(row_number, column_name){
            return this.generatedData[column_name][row_number]
        }
    },
    template: `
    <div class="container-fluid" style="height:100%">
            <div class="row" style="padding-top:10px">
                <div v-for="column in tableColumns" class="col-xs-1" style="font-weight: bold;">
                        {{column}}
                </div>
            </div>
            <div class="container-fluid generated-data" style="height:80%;">
                <div v-for="row_number in numberOfRows" class="row data-row">
                    <div v-for="column in tableColumns" class="col-xs-1 column-data">
                        <div v-if="column==selectedColumn" style="font-weight:bold;">
                            {{getCellData(row_number,column)}}
                        </div>
                        <div v-else>
                            {{getCellData(row_number,column)}}
                        </div>
                    </div>
                </div>
            </div>
    </div>

    `      
}