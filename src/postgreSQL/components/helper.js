
import { normalDistribution } from "./genForms/normalDistribution.js"

import { uniformDistribution } from "./genForms/uniformDistribution.js"

import { pg_string_types,number_distribution_types,pg_date_types,pg_datetime_types,pg_number_types,string_fill_types } from "./constants.js"

var path = require("path")
const db = require(path.join(__dirname,"/db/index.js"))


let first_names = ["Ada", "Adam", "Adrian", "Agnieszka", "Ainhoa", "Al", "Alba", "Albert", "Alejandro", "Alex", "Alexander", "Alexis", "Alice", "Alva", "Alvaro", "Amber", "Amy", "Ana", "Andy", "Andrea", "Andrew", "Andrzej", "Ann", "Anna", "Anne", "Annie", "Anthony", "Anton", "Aoife", "Ashley", "Barbara", "Ben", "Bengie", "Benjamin", "Benjy", "Bert", "Bess", "Betty", "Bill", "Bob", "Bram", "Caitlin", "Callum", "Cameron", "Camila", "Camilla", "Carla", "Carlos", "Carolina", "Caroline", "Cath", "Catharine", "Catherine", "Cathy", "Cecilie", "Charles", "Charlie", "Charlotte", "Chloe", "Christian", "Christopher", "Chuck", "Cian", "Ciara", "Claudia", "Conor", "Cristian", "Cristina", "Daan", "Daniel", "Daniela", "Dave", "David", "Dick", "Diego", "Dylan", "Dorothy", "Drew", "Duncan", "Ed", "Edward", "Edwyn", "Eleanor", "Elena", "Elin", "Elizabeth", "Ellie", "Elzbieta", "Emil", "Emily", "Emma", "Erik", "Erin", "Ewa", "Fabian", "Femke", "Filip", "Florian", "Francisco", "Frederik", "Freja", "Gabbie", "Gabrielle", "Geoff", "Geoffery", "George", "Georgina", "Gill", "Gillian", "Godfrey", "Hank", "Hanna", "Hannah", "Harold", "Harry", "Helen", "Henry", "Herb", "Herbert", "Hiram", "Hugo", "Ida", "Ike", "Iris", "Isaac", "Isabel", "Isabelle", "Isak", "Ivan", "Izzy", "Jace", "Jack", "Jaclyn", "Jacob", "James", "Jamie", "Jan", "Jane", "Janet", "Javier", "Jean", "Jeanette", "Jeffery", "Jesse", "Jessica", "Jill", "Jim", "Jo", "Joanne", "Joe", "Joey", "John", "Johnny", "Jonas", "Jorge", "Jose", "Joseph", "Joshua", "Jozef", "Juan", "Juana", "Julia", "Julian", "Kay", "Kaylee", "Kayleigh", "Karen", "Karin", "Katarzyna", "Katharina", "Katie", "Kim", "Kimberly", "Kimmy", "Klara", "Krystyna", "Krzysztof", "Lara", "Lars", "Laura", "Lauren", "Lea", "Leah", "Lena", "Leon", "Leonie", "Lewis", "Liam", "Lincoln", "Linnea", "Lisa", "Liza", "Lizzy", "Lotte", "Louise", "Lu", "Luca", "Lucas", "Lucia", "Lucille", "Lucy", "Luis", "Lukas", "Luke", "Madison", "Mads", "Maggie", "Magnus", "Maja", "Malgorzata", "Manuel", "Marcin", "Marek", "Margaret", "Margarita", "Maria", "Marie", "Mario", "Mary", "Marta", "Martin", "Marty", "Mathias", "Mathilde", "Matt", "Matthew", "Max", "Maximilian", "Megan", "Michael", "Mick", "Mike", "Mikkel", "Milan", "Miriam", "Nate", "Nathan", "Nerea", "Nicholas", "Nick", "Nicky", "Nicolas", "Nigel", "Niklas", "Oliver", "Olivia", "Ollie", "Oscar", "Pablo", "Patricia", "Paul", "Paula", "Pawel", "Pedro", "Peg", "Peggy", "Pete", "Peter", "Phil"];
let first_names_cro = ["Luka", "David", "Jakov", "Ivan", "Roko", "Petar", "Mateo", "Niko", "Matej", "Fran", "Josip", "Noa", "Mihael", "Borna", "Toma", "Filip", "Leon", "Karlo", "Marko", "Lovro", "Jan", "Ivano", "Vito", "Šimun", "Teo", "Lukas", "Ante", "Nikola", "Gabriel", "Leo", "Mia", "Lucija", "Nika", "Rita", "Ema", "Mila", "Marta", "Sara", "Ana", "Dora", "Eva", "Elena", "Lana", "Petra", "Iva", "Klara", "Lara", "Marija", "Lea", "Han", "Ena", "Franka", "Tena", "Leona", "Laura", "Emili", "Maša", "Una", "Vita", "Lena"]
let last_names = ["Franklin", "Brisco", "Willis", "Mejia", "Manson", "Spensley", "Moore", "Slemp", "Daughtery", "Otto", "White", "Voigt", "Clarke", "Bloom", "Jackson", "Wesolowski", "Mayberry", "Ratliff", "Hardoon", "Evans", "Jones", "Harder", "Petrzelka", "Raines", "Stockton", "Green", "DeWald", "Shapiro", "Sakurai", "Deans", "McDaniel", "Liddle", "Pyland", "Troher", "Hedgecock", "Frega", "Stewart", "Foreman", "Williams", "Gieske", "Imhoff", "Haynes", "Slocum", "Knopp", "Jones", "Chapman", "Brown", "Deleo", "DelRosso", "Tudisco", "Davis", "Archer", "Cappello", "Wilson", "Sterrett", "Chwatal", "Buchholz", "Helfrich", "Anderson", "Bruno", "Gildersleeve", "Moreau", "Bryant", "Symms", "Angarano", "Antonucci", "Robinson", "Pearlman", "Helbush", "Keller", "Massingill", "Overton", "Wong", "Thompson", "Seibel", "Pierce", "Stevens", "Little", "Griffith", "Yinger", "Dulisse", "Conley", "King", "Nahay", "Press", "Depew", "Fox", "Linhart", "Rivers", "Waddell", "Gibson", "Brumley", "Caouette", "Scheffold", "Freed", "Linton", "Moon", "Bergdahl", "Mitchell", "Gaskins", "Ray", "Boyer", "McCrary", "Riegel", "Williamson", "Weaver", "Laudanski", "Cramer", "Durso", "Allison", "Aldritch", "Paddock", "Huston", "Arcadi", "Poissant", "Lawton", "Stockton", "Freed", "Shapiro", "Hoyt", "Ionescu", "Wood", "Pickering", "White", "Queen", "Kidd", "Long", "Herzog", "Ostanik", "Hamilton", "Wilson", "Nobles", "Hankins", "Jessen", "King", "Morton", "Young", "Morton", "Lee", "Emerson", "Miller", "Kellock", "Stevenson", "Dean", "Hopper", "Ayers", "Harness", "Bogdanovich", "Wargula", "Mcnally", "Freeman", "Zia", "Comeau", "Newman", "Zurich", "Kingslan", "Mitchell", "Watson", "Brady", "Royal", "Koch", "Oyler", "Lamere", "Morgan", "Clark", "Knight", "Wakefield", "Plantz", "Poplock", "Toler"];
let last_names_cro = ["Knežević", "Horvat", "Kovačević", "Pavlović", "Blažević", "Božić", "Lovrić", "Babić", "Marković", "Bošnjak", "Grgić", "Brkić", "Filipović", "Vidović", "Kovačić", "Tomić", "Jukić", "Novak", "Martinović", "Petrović", "Mandić"]
let cities = ["Abidjan", "Adana", "ADDIS ABABA", "Adelaide", "Ahmedabad", "Alexandria", "Algiers", "Almaty", "AMMAN", "ANKARA", "Anshan", "BAGHDAD", "BAKU", "Bandung", "Bangalore", "BANGKOK", "Baotou", "Barcelona", "Barranquilla", "BEIJING", "Belm", "Belo Horizonte", "Belgrade", "BERLIN", "Bhopal", "Birmingham", "Bogor", "BOGOTA", "Bombay", "BRASILIA", "Brisbane", "BUCURESTI", "BUDAPEST", "BUENOS AIRES", "Bursa"]
let cities_cro = ["Bakar", "Beli Manastir", "Belišće", "Benkovac", "Biograd na Moru", "Bjelovar", "Buje", "Buzet", "Cres", "Crikvenica", "Čabar", "Čakovec", "Čazma", "Daruvar", "Delnice", "Donja Stubica", "Donji Miholjac", "Drniš", "Dubrovnik", "Duga Resa", "Dugo Selo", "Đakovo", "Đurđevac", "Garešnica", "Glina", "Gospić", "Grubišno Polje", "Hrvatska Kostajnica", "Hvar", "Ilok", "Imotski", "Ivanec", "Ivanić-Grad", "Jastrebarsko", "Karlovac", "Kastav", "Kaštela", "Klanjec", "Knin", "Komiža", "Koprivnica", "Korčula", "Kraljevica", "Krapina", "Križevci", "Krk", "Kutina", "Kutjevo", "Labin", "Lepoglava", "Lipik", "Ludbreg", "Makarska", "Mali Lošinj", "Metković", "Mursko Središće", "Našice", "Nin", "Nova Gradiška", "Novalja", "Novigrad", "Novi Marof", "Novi Vinodolski", "Novska", "Obrovac", "Ogulin", "Omiš", "Opatija", "Opuzen", "Orahovica", "Oroslavje", "Osijek", "Otočac", "Otok", "Ozalj", "Pag", "Pakrac", "Pazin", "Petrinja", "Pleternica", "Ploče", "Popovača", "Poreč", "Požega", "Pregrada", "Prelog", "Pula", "Rab", "Rijeka", "Rovinj", "Samobor", "Senj", "Sinj", "Sisak", "Skradin", "Slatina", "Slavonski Brod", "Slunj", "Solin", "Split", "Stari Grad", "Supetar", "Sveta Nedelja", "Sveti Ivan Zelina", "Šibenik", "Trilj", "Trogir", "Umag", "Valpovo", "Varaždin", "Varaždinske Toplice", "Velika Gorica", "Vinkovci", "Virovitica", "Vis", "Vodice", "Vodnjan", "Vrbovec", "Vrbovsko", "Vrgorac", "Vrlika", "Vukovar", "Zabok", "Zadar", "Zagreb", "Zaprešić", "Zlatar", "Županja"]

let languages = ["Abanyom", "Abaza", "Abenaki", "Abkhaz", "Abujmaria", "Acehnese", "Adamorobe Sign Language", "Adele", "Adyghe", "Afar", "Afrikaans", "Afro-Seminole Creole", "Aimaq", "Aini", "Ainu", "Akan", "Akawaio", "Akkadian", "Aklanon", "Albanian", "Aleut", "Algonquin", "Alem�n Coloneiro", "Alsatian", "Altay", "Alutor", "American Sign Language", "Amharic", "Amorite", "Anglo-Saxon", "Amdang", "Ammonite", "Andalusian", "Angaur", "Angika", "Anyin", "Ao", "A-Pucikwar", "Arabic", "Aragonese", "Aramaic", "Are", "Argobba", "Aromanian", "Armenian", "Arvanitic", "Ashkenazi Hebrew", "Ashkun", "Assamese", "Assyrian Neo-Aramaic", "Ateso or Teso", "Asi", "Asturian", "Ati", "Auslan", "Avar", "Avestan", "Awadhi", "Aymara", "Azerbaijani", "Badaga", "Badeshi", "Bahnar", "Bajelani", "Balinese", "Balochi", "Balti", "Bambara", "Banjar", "Banyumasan", "Basaa", "Bashkir", "Basque", "Batak Dairi", "Batak Karo", "Batak Mandailing", "Batak Simalungun", "Batak Toba", "Bateri", "Bats", "Bavarian", "Beja", "Belarusian", "Belhare", "Bellari", "Berta", "Bemba", "Bengali", "Bengali Sign language", "Bezhta", "Beothuk", "Berber", "Bete", "B�t�", "Bhili", "Bhojpuri", "Bijil Neo-Aramaic", "Bikol", "Bikya", "Bissa", "Blackfoot", "Boholano", "Bohtan Neo-Aramaic", "Bolgar", "Bonan", "Bororo", "Bosnian", "Brahui", "Breton", "British Sign Language", "Bua", "Buginese", "Bukusu", "Bulgarian", "Bunjevac", "Burmese", "Burushaski", "Buryat", "Caluyanon", "Camunic", "Cantonese", "Carian", "Catawba", "Catalan", "Cayuga", "Cebuano", "Chabacano", "Chaga", "Chagatai", "Chaldean Neo-Aramaic", "Chamorro", "Chaouia", "Chechen", "Chemakum", "Chenchu", "Chenoua", "Cherokee", "Cheyenne", "Chhattisgarhi", "Chickasaw", "Chintang", "Chilcotin", "Chinese", "Chiricahua", "Chichewa", "Chipewyan", "Chittagonian", "Choctaw"];
let currencies = ["AED", "AFN", "ALL", "AMD", "ANG", "AOA", "ARS", "AUD", "AWG", "AZN", "BAM", "BBD", "BDT", "BGN", "BHD", "BIF", "BMD", "BND", "BOB", "BOV", "BRL", "BSD", "BTN", "BWP", "BYN", "BZD", "CAD", "CDF", "CHE", "CHF", "CHW", "CLF", "CLP", "CNY", "COP", "COU", "CRC", "CUC", "CUP", "CVE", "CZK", "DJF", "DKK", "DOP", "DZD", "EGP", "ERN", "ETB", "EUR", "FJD", "FKP", "GBP", "GEL", "GHS", "GIP", "GMD", "GNF", "GTQ", "GYD", "HKD", "HNL", "HRK", "HTG", "HUF", "IDR", "ILS", "INR", "IQD", "IRR", "ISK", "JMD", "JOD", "JPY", "KES", "KGS", "KHR", "KMF", "KPW", "KRW", "KWD", "KYD", "KZT", "LAK", "LBP", "LKR", "LRD", "LSL", "LYD", "MAD", "MDL", "MGA", "MKD", "MMK", "MNT", "MOP", "MRU", "MUR", "MVR", "MWK", "MXN", "MXV", "MYR", "MZN", "NAD", "NGN", "NIO", "NOK", "NPR", "NZD", "OMR", "PAB", "PEN", "PGK", "PHP", "PKR", "PLN", "PYG", "QAR", "RON", "RSD", "RUB", "RWF", "SAR", "SBD", "SCR", "SDG", "SEK", "SGD", "SHP", "SLL", "SOS", "SRD", "SSP", "STN", "SVC", "SYP", "SZL", "THB", "TJS", "TMT", "TND", "TOP", "TRY", "TTD", "TWD", "TZS", "UAH", "UGX", "USD", "USN", "UYI", "UYU", "UYW", "UZS", "VED", "VES", "VND", "VUV", "WST", "XAF", "XAG", "XAU", "XBA", "XBB", "XBC", "XBD", "XCD", "XDR", "XOF", "XPD", "XPF", "XPT", "XSU", "XTS", "XUA", "XXX", "YER", "ZAR", "ZMW", "ZWL"]
let colors = ["Alice blue", "Alizarin", "Amaranth", "Amber", "Amethyst", "Apricot", "Aqua", "Aquamarine", "Army green", "Asparagus", "Auburn", "Azure", "Baby blue", "Beige", "Bistre", "Black", "Blue", "Blue-green", "Blue-violet", "Bondi blue", "Brass", "Bright green", "Bright turquoise", "Brilliant rose", "Bronze", "Brown", "Buff", "Burgundy", "Burnt orange", "Burnt sienna", "Burnt umber", "Camouflage green", "Caput Mortuum", "Cardinal", "Carmine", "Carnation pink", "Carolina blue", "Carrot orange", "Celadon", "Cerise", "Cerulean", "Cerulean blue", "Chartreuse", "Chestnut", "Chocolate", "Cinnabar", "Cinnamon", "Cobalt", "Copper", "Copper rose", "Coral", "Coral red", "Corn", "Cornflower blue", "Cosmic latte", "Cream", "Crimson", "Cyan", "Dark blue", "Dark brown", "Dark cerulean", "Dark chestnut", "Dark coral", "Dark goldenrod", "Dark green", "Dark khaki", "Dark pastel green", "Dark pink", "Dark salmon", "Dark slate gray", "Dark spring green", "Dark tan", "Dark tangerine", "Dark turquoise", "Dark violet", "Deep cerise", "Deep fuchsia", "Deep lilac", "Deep magenta", "Deep peach", "Deep pink", "Denim", "Dodger blue", "Ecru", "Electric blue", "Electric green", "Electric indigo", "Electric lime", "Electric purple", "Emerald", "Eggplant", "Falu red", "Fern green", "Firebrick", "Flax", "Forest green", "French Rose", "Fuchsia", "Fuchsia Pink", "Gamboge", "Gold", "Golden brown", "Golden yellow", "Goldenrod", "Grey-asparagus", "Green", "Grey", "Han Purple", "Harlequin", "Heliotrope", "Hollywood Cerise", "Hot Magenta", "Hot Pink", "Indigo", "Ivory", "Jade", "Khaki", "Lavender", "Lawn green", "Lemon", "Lime", "Linen", "Magenta", "Malachite", "Maroon", "Maya blue", "Mauve", "Mauve Taupe", "Midnight Blue", "Mint Green", "Misty rose", "Moss green", "Mountbatten pink", "Mustard", "Myrtle", "Navajo white", "Navy Blue", "Ochre", "Office green", "Old Gold", "Old Lace", "Old Lavender", "Old Rose", "Olive", "Olive Drab", "Olivine", "Orange", "Orchid", "Pastel green", "Pastel pink", "Peach", "Pear", "Periwinkle", "Persian blue", "Persian green", "Persian indigo", "Persian red", "Persian pink", "Persian rose", "Persimmon", "Pink", "Platinum", "Puce", "Pumpkin", "Purple", "Raw umber", "Red", "Red-violet", "Rich carmine", "Rich magenta", "Robin egg blue", "Rose", "Rose Taupe", "Royal purple", "Ruby", "Russet", "Rust", "Saffron", "Salmon", "Sandy brown", "Sangria", "Sapphire", "Scarlet", "Sea Green", "Seashell", "Sepia", "Shamrock green", "Silver", "Sky Blue", "Slate grey", "Spring bud", "Spring green", "Steel blue", "Tan", "Tangerine", "Tea Green", "Tea rose", "Teal", "Terra cotta", "Thistle", "Turquoise", "Tyrian purple", "Vermilion", "Violet", "Wheat", "White", "Wisteria", "Yellow", "Zinnwaldite"]

export async function getTablesAndColumnsDevelopment() { 
    return {
      Library: {
        table_name: 'Library',
        selected: false,
        collapsed: true,
        number_of_rows: 100,
        data_generated: false,
        visited: false,
        columns: {
          Id: {
            column_name: 'Id',
            data_type: 'integer',
            character_maximum_length: null,
            is_nullable: 'NO',
            is_primary_key: true,
            is_foreign_key: false,
            generator_settings: {
              type: 'Number',
              generator: 'Sequential',
              null_fill_factor: 0
            }
          },
          City: {
            column_name: 'City',
            data_type: 'character varying',
            character_maximum_length: null,
            is_nullable: 'YES',
            is_primary_key: false,
            is_foreign_key: false,
            generator_settings: { type: 'String', generator: 'City', null_fill_factor: 0 }
          },
          Name: {
            column_name: 'Name',
            data_type: 'character varying',
            character_maximum_length: null,
            is_nullable: 'YES',
            is_primary_key: false,
            is_foreign_key: false,
            generator_settings: {
              type: 'String',
              generator: 'Random',
              string_length: 5,
              null_fill_factor: 0
            }
          }
        },
        foreign_keys_in_table: {
          table_dependencies: [],
          foreign_keys_dict: {},
          foreign_keys_list: []
        }
      },
      Book: {
        table_name: 'Book',
        selected: false,
        collapsed: true,
        number_of_rows: 100,
        data_generated: false,
        visited: false,
        columns: {
          Id: {
            column_name: 'Id',
            data_type: 'integer',
            character_maximum_length: null,
            is_nullable: 'NO',
            is_primary_key: true,
            is_foreign_key: false,
            generator_settings: {
              type: 'Number',
              generator: 'Sequential',
              null_fill_factor: 0
            }
          },
          Library: {
            column_name: 'Library',
            data_type: 'bigint',
            character_maximum_length: null,
            is_nullable: 'NO',
            is_primary_key: false,
            is_foreign_key: true,
            generator_settings: {
              type: 'Number',
              generator: 'Uniform',
              null_fill_factor: 0,
              start_range: 0,
              end_range: 100
            }
          },
          Name: {
            column_name: 'Name',
            data_type: 'character varying',
            character_maximum_length: null,
            is_nullable: 'YES',
            is_primary_key: false,
            is_foreign_key: false,
            generator_settings: {
              type: 'String',
              generator: 'Random',
              string_length: 5,
              null_fill_factor: 0
            }
          },
          Author: {
            column_name: 'Author',
            data_type: 'character varying',
            character_maximum_length: null,
            is_nullable: 'YES',
            is_primary_key: false,
            is_foreign_key: false,
            generator_settings: {
              type: 'String',
              generator: 'Random',
              string_length: 5,
              null_fill_factor: 0
            }
          }
        },
        foreign_keys_in_table: {
          table_dependencies: [ 'Library' ],
          foreign_keys_dict: { Library: { table: 'Library', column: 'Id' } },
          foreign_keys_list: [ 'Library' ]
        }
      },
      Member: {
        table_name: 'Member',
        selected: false,
        collapsed: true,
        number_of_rows: 100,
        data_generated: false,
        visited: false,
        columns: {
          Id: {
            column_name: 'Id',
            data_type: 'bigint',
            character_maximum_length: null,
            is_nullable: 'NO',
            is_primary_key: true,
            is_foreign_key: false,
            generator_settings: {
              type: 'Number',
              generator: 'Sequential',
              null_fill_factor: 0
            }
          },
          Library: {
            column_name: 'Library',
            data_type: 'bigint',
            character_maximum_length: null,
            is_nullable: 'YES',
            is_primary_key: false,
            is_foreign_key: true,
            generator_settings: {
              type: 'Number',
              generator: 'Uniform',
              null_fill_factor: 0,
              start_range: 0,
              end_range: 100
            }
          },
          FirstName: {
            column_name: 'FirstName',
            data_type: 'character varying',
            character_maximum_length: null,
            is_nullable: 'YES',
            is_primary_key: false,
            is_foreign_key: false,
            generator_settings: {
              type: 'String',
              generator: 'First Name',
              null_fill_factor: 0
            }
          },
          LastName: {
            column_name: 'LastName',
            data_type: 'character varying',
            character_maximum_length: null,
            is_nullable: 'YES',
            is_primary_key: false,
            is_foreign_key: false,
            generator_settings: { type: 'String', generator: 'Last Name', null_fill_factor: 0 }
          }
        },
        foreign_keys_in_table: {
          table_dependencies: [ 'Library' ],
          foreign_keys_dict: { Library: { table: 'Library', column: 'Id' } },
          foreign_keys_list: [ 'Library' ]
        }
      }
    }
}

export async function getTablesAndColumns() {
  //get table names
  const queryTableNamesLike = {
      name : "tables",
      text : `SELECT table_name
      FROM information_schema.tables
     WHERE table_schema='public'
       AND table_type='BASE TABLE';`
  }

  let tableColumns = {}

  let result = await db.query(queryTableNamesLike);
  

  let primary_keys_in_column = {}
  let foreign_keys_in_table = {}
  
  //get table columns
  let result2;
  for (const table in result) {
      let primary_key_list = []
      let columns = {
          text : `SELECT column_name, data_type, character_maximum_length, is_nullable 
          FROM information_schema.columns
         WHERE table_name = '${result[table].table_name}';`
      }
      result2 = await db.query(columns);
      tableColumns[result[table].table_name] = result2

      //get primary keys

      let table_name = result[table].table_name
      let primary_keys = {
          text : `SELECT c.column_name
          FROM information_schema.table_constraints tc 
          JOIN information_schema.constraint_column_usage AS ccu USING (constraint_schema, constraint_name) 
          JOIN information_schema.columns AS c ON c.table_schema = tc.constraint_schema
            AND tc.table_name = c.table_name AND ccu.column_name = c.column_name
          WHERE constraint_type = 'PRIMARY KEY' and tc.table_name = '${table_name}'`
      }
      result2 = await db.query(primary_keys);
  
      for (const key in result2) {
          if (Object.hasOwnProperty.call(result2, key)) {
              primary_key_list.push(result2[key].column_name)
          }
      }
      primary_keys_in_column[table_name] = primary_key_list

      let foreign_keys = {
          text : `
                  SELECT
              tc.table_schema, 
              tc.constraint_name, 
              tc.table_name, 
              kcu.column_name, 
              ccu.table_schema AS foreign_table_schema,
              ccu.table_name AS foreign_table_name,
              ccu.column_name AS foreign_column_name 
          FROM 
              information_schema.table_constraints AS tc 
              JOIN information_schema.key_column_usage AS kcu
              ON tc.constraint_name = kcu.constraint_name
              AND tc.table_schema = kcu.table_schema
              JOIN information_schema.constraint_column_usage AS ccu
              ON ccu.constraint_name = tc.constraint_name
              AND ccu.table_schema = tc.table_schema
          WHERE tc.constraint_type = 'FOREIGN KEY' AND  tc.table_name = '${result[table].table_name}' ;`
      }
      let result3 = await db.query(foreign_keys);

      let fks = result3

      let table_dependencies = []
      let foreign_keys_dict = {}
      let foreign_keys_list = []

      for(let index = 0; index < fks.length; index++) {
          foreign_keys_dict[fks[index]['column_name']] = {
              'table' : fks[index]['foreign_table_name'],
              'column' : fks[index]['foreign_column_name']
          }
          foreign_keys_list.push(fks[index]['column_name'])
          table_dependencies.push(fks[index]['foreign_table_name'])
      }
      
      foreign_keys_in_table[result[table].table_name] = {
          'table_dependencies' : table_dependencies,
          'foreign_keys_dict' : foreign_keys_dict,
          'foreign_keys_list' : foreign_keys_list
      }
      
  }
  //console.log(tableColumns)

  var construct = {}
  
  for (const key in tableColumns) {
      if (Object.hasOwnProperty.call(tableColumns, key)) {
          const element = tableColumns[key];

          construct[key] = {}
          construct[key]['table_name'] = key
          construct[key]['selected'] = false
          construct[key]['collapsed'] = true
          construct[key]['number_of_rows'] = 100
          construct[key]['data_generated'] = false
          construct[key]['visited'] = false
          construct[key]['columns'] = {}

          construct[key]['foreign_keys_in_table'] = foreign_keys_in_table[key]
          
          var j = 0
          for (const column in element) {
              construct[key]['columns'][element[column].column_name] = {}
              construct[key]['columns'][element[column].column_name]['column_name'] = element[column].column_name
              construct[key]['columns'][element[column].column_name]['data_type'] = element[column].data_type
              construct[key]['columns'][element[column].column_name]['character_maximum_length'] = element[column].character_maximum_length
              construct[key]['columns'][element[column].column_name]['is_nullable'] = element[column].is_nullable
              construct[key]['columns'][element[column].column_name]['is_primary_key'] = false
              construct[key]['columns'][element[column].column_name]['is_foreign_key'] = false
              //check if primary key
              if(primary_keys_in_column[key].includes(element[column].column_name)) {
                  construct[key]['columns'][element[column].column_name]['is_primary_key'] = true
              }
              //check if foreign key
              if(foreign_keys_in_table[key]!= null && foreign_keys_in_table[key]['foreign_keys_list'].includes(element[column].column_name)) {
                  construct[key]['columns'][element[column].column_name]['is_foreign_key'] = true
              }
              //maybe set default generator_settings here
              construct[key]['columns'][element[column].column_name]['generator_settings'] = 
                  getDefaultGeneratorSettings(element[column].column_name, element[column].data_type, 
                       construct[key]['columns'][element[column].column_name]['is_primary_key'],
                       construct[key]['columns'][element[column].column_name]['character_maximum_length'])
              
              ++j
          }
      }

  }
  require('util').inspect.defaultOptions.depth = null
  //console.log(JSON.stringify(construct))
  return construct
}

//beefy function
function getDefaultGeneratorSettings(table_name, data_type, is_primary_key, character_maximum_length) {
  let generator_settings = {}
  //string data type
  if(pg_string_types.includes(data_type)){
      generator_settings['type'] = 'String';
       if(table_name.match(/firstname/gi) != null){
          generator_settings['generator'] = 'First Name'
          generator_settings['null_fill_factor'] = 0
      }
      else if(table_name.match(/lastname/gi) != null){
          generator_settings['generator'] = 'Last Name'
          generator_settings['null_fill_factor'] = 0
      }
      else if(table_name.match(/city/gi) != null){
          generator_settings['generator'] = 'City'
          generator_settings['null_fill_factor'] = 0
      }
      else if(table_name.match(/color/gi) != null){
          generator_settings['generator'] = 'Color'
          generator_settings['null_fill_factor'] = 0
      }
      else if(table_name.match(/currency/gi) != null){
          generator_settings['generator'] = 'Currency'
          generator_settings['null_fill_factor'] = 0
      }
      else if(table_name.match(/language/gi) != null){
          generator_settings['generator'] = 'Language'
          generator_settings['null_fill_factor'] = 0
      }
      else {
          generator_settings['generator'] = 'Random'
          if(character_maximum_length != null) {
              generator_settings['string_length'] = character_maximum_length
          } else {
              generator_settings['string_length'] = 5
          }
          generator_settings['null_fill_factor'] = 0      
      }
  }
  //number data type
  if(pg_number_types.includes(data_type)){ 
      generator_settings['type'] = 'Number';
      if(is_primary_key) {
          generator_settings['generator'] = 'Sequential'
          generator_settings['null_fill_factor'] = 0
      }
      else {
          generator_settings['generator'] = 'Uniform'
          generator_settings['null_fill_factor'] = 0
          generator_settings['start_range'] = 0
          generator_settings['end_range'] = 100
      }
  }
  //date
  if(pg_date_types.includes(data_type)){
      generator_settings['type'] = 'Date';
      generator_settings['date_from'] = '2000-01-01'
      generator_settings['date_to'] = '2005-01-01'
      generator_settings['valid_dates'] = true
      generator_settings['null_fill_factor'] = 0
  }
  //datetime
  if(pg_datetime_types.includes(data_type)){
      generator_settings['type'] = 'Datetime'; 
      generator_settings['datetime_from'] = '2000-01-01T22:00'
      generator_settings['datetime_to'] = '2005-05-01T22:00'
      generator_settings['valid_dates'] = true
      generator_settings['null_fill_factor'] = 0
  }
  return generator_settings;

}


function generateRandomString(length) {
  var result           = '';
  var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for ( var i = 0; i < length; i++ ) {
    result += characters.charAt(Math.floor(Math.random() *  charactersLength));
 }
 return result;
}

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

function randomDate(start, end, startHour, endHour) {
  var date = new Date(+start + Math.random() * (end - start));
  var hour = startHour + Math.random() * (endHour - startHour) | 0;
  date.setHours(hour);
  return date;
}

function formatDate(date) {
  return date.getDate() + '-' + (date.getMonth()+1) + '-' + date.getFullYear()
}

export function generateDataForColumn(generator_settings, number_of_rows,tableName, columnName ,pgMetadata) {
  let data = []
  let is_cell_null = false
  let null_fill_factor = generator_settings['null_fill_factor']

  if(pgMetadata[tableName]['columns'][columnName]['is_foreign_key']) {
    generator_settings['start_range'] = 1

    //get dependancy
    let fk_table = pgMetadata[tableName]['foreign_keys_in_table']['foreign_keys_dict'][columnName]['table']
    generator_settings['end_range'] = pgMetadata[fk_table]['number_of_rows'] - 1
  }

  
  for (let i = 0; i < number_of_rows; i++) {

    is_cell_null = getRandomInt(0,100) < null_fill_factor

    if(is_cell_null){
      data.push('NULL')
      continue
    }

    if(generator_settings['type'] == 'String') {
      let file_data
      if(generator_settings['generator'] == 'Random') {
        data.push(generateRandomString(generator_settings['string_length']))
      }
      else if (generator_settings['generator'] == 'First Name') {
        if(file_data == null) {
          file_data = first_names
        }
        data.push(file_data[Math.floor(Math.random() * file_data.length)])
      }
      else if (generator_settings['generator'] == 'First Name (CRO)') {
        if(file_data == null) {
          file_data = first_names_cro
        }
        data.push(file_data[Math.floor(Math.random() * file_data.length)])
      }
      else if (generator_settings['generator'] == 'Last Name') {
        if(file_data == null) {
          file_data = last_names
        }
        data.push(file_data[Math.floor(Math.random() * file_data.length)]) 
      }
      else if (generator_settings['generator'] == 'Last Name (CRO)') {
        if(file_data == null) {
          file_data = last_names_cro
        }
        data.push(file_data[Math.floor(Math.random() * file_data.length)]) 
      }
      else if (generator_settings['generator'] == 'City') {
        if(file_data == null) {
          file_data = cities
        }
        data.push(file_data[Math.floor(Math.random() * file_data.length)])       
      }
      else if (generator_settings['generator'] == 'City (CRO)') {
        if(file_data == null) {
          file_data = cities_cro
        }
        data.push(file_data[Math.floor(Math.random() * file_data.length)])       
      }
      else if (generator_settings['generator'] == 'Language') {
        if(file_data == null) {
          file_data = languages;
        }
        data.push(file_data[Math.floor(Math.random() * file_data.length)])       
      }
      else if (generator_settings['generator'] == 'Currency') {
        if(file_data == null) {
          file_data = currencies;
        }
        data.push(file_data[Math.floor(Math.random() * file_data.length)])       
      }
      else if (generator_settings['generator'] == 'Color') {
        if(file_data == null) {
          file_data = colors;
        }
        data.push(file_data[Math.floor(Math.random() * file_data.length)])       
      }
      else if (generator_settings['generator'] == 'Constants') {
        let constants = generator_settings['constant_string'].split(";")
        data.push(constants[getRandomInt(0,constants.length)])
      }
      else if (generator_settings['generator'] == 'Regex') {
        let regex_pattern = generator_settings['regex_pattern']
        let max_regex_size = generator_settings['string_length'] != null ? generator_settings['string_length'] : regex_pattern.length
        let regex = ''
        for (let index = 0; index < max_regex_size; index++) {
            if(regex_pattern.charAt(index) == ''){
              break
            }
            if(regex_pattern.charAt(index) == '_'){
              regex += generateRandomString(1)
            }
            else{
              regex += regex_pattern.charAt(index)
            }
        }
        data.push(regex)
      }
      else {
        data.push(null)
      }
    }
    else if(generator_settings['type'] == 'Number') {
      //generate random numbers for now...
      if(generator_settings['generator'] == 'Sequential') {
        data.push(i)
      }
      else if(generator_settings['generator'] == 'Normal'){
        let generatedData = normalDistribution(generator_settings['start_range'], generator_settings['end_range'], number_of_rows)

        let counter = 0
        let number
        let random_number
        while(counter <= number_of_rows) {
          random_number = getRandomInt(generator_settings['start_range'], generator_settings['end_range']) 
          number = generatedData[random_number]
          if(number == 0) {
            delete generatedData[random_number]
            continue
          } else {
            data.push(random_number)
            generatedData[random_number] = number - 1
            ++counter
          }
        }
        break
      }
      else if(generator_settings['generator'] == 'Uniform') {
        let generatedData = uniformDistribution(generator_settings['start_range'], generator_settings['end_range'], number_of_rows)

        let counter = 0
        let number
        let random_number
        while(counter <= number_of_rows) {
          random_number = getRandomInt(generator_settings['start_range'], generator_settings['end_range']) 
          number = generatedData[random_number]
          if(number == 0) {
            delete generatedData[random_number]
            continue
          } else {
            data.push(random_number)
            generatedData[random_number] = number - 1
            ++counter
          }
        }
        break    
      }
      else {
        data.push(getRandomInt(generator_settings['start_range'], generator_settings['end_range'] ))
      }
    }
    else if(generator_settings['type'] == 'Date') {
      data.push(randomDate(new Date(generator_settings['date_from']), new Date(generator_settings['date_to']), 0, 21).toJSON().slice(0,10))
    }
    else if(generator_settings['type'] == 'Datetime') {
      data.push(randomDate
        (new Date(generator_settings['datetime_from'].substring(0,10)), 
        new Date(generator_settings['datetime_to'].substring(0,10)),
        generator_settings['datetime_from'].substring(11,13),
        23
        ))
    }
    else {
      data.push(null);
    }
  }
  return data
}