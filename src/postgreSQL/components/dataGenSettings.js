import * as constants from './constants.js'
import stringGenForm from './genForms/stringGenForm.js'
import dateGenForm from './genForms/dateGenForm.js'
import datetimeGenForm from './genForms/datetimeGenForm.js'
import numberGenForm from './genForms/numberGenForm.js'
import tableGenForm from './genForms/tableGenForm.js'

export default {
    components: {
        stringGenForm,
        dateGenForm,
        datetimeGenForm,
        numberGenForm,
        tableGenForm
    },
    emits: ['updateTableColumnGenSettings', 'updateTableSettings'],
    props: {
        selectedRowType : String,
        selectedTable : String,
        selectedColumn : String,
        numberOfTableRows : Number,
        selectedColumnProperties : Object,
        selectedColumnDataGenSettings : Object,
        reRenderKey: Number
    },
    data() {},
    watch : {},
    methods : {
    updateColumnGenSettings(genSettings) {
      this.$emit('updateTableColumnGenSettings',this.selectedTable, this.selectedColumn ,genSettings)  
    },
    updateTableSettings(number_of_rows) {
      this.$emit('updateTableSettings', number_of_rows)  
    },
    getColumnRootDataType() {
        if(this.selectedColumnProperties == null) {
            return null
        }
        if(constants.pg_string_types.includes(this.selectedColumnProperties['data_type'])) {
            return 'String'
        }
        if(constants.pg_date_types.includes(this.selectedColumnProperties['data_type'])) {
          return 'Date'
        }
        if(constants.pg_datetime_types.includes(this.selectedColumnProperties['data_type'])) {
          return 'Datetime'
        }
        if(constants.pg_number_types.includes(this.selectedColumnProperties['data_type'])) {
          return 'Integer'
        }
    }
    },
    template: `
    <div class="container-fluid">

        <div v-if="selectedRowType== '' || selectedRowType==Null " class="row">
          <div class="col-xs-12">
              <h3 style="padding-top:10px">Select Table or Column</h3>
          </div>          
        </div>

        <div v-if="selectedRowType=='Column'" class="row">
          <div class="col-xs-12">
            <h3 style="padding-top:10px">Settings for {{selectedTable}} - {{selectedColumn}}</h3>
          </div> 
        </div>

        <div v-if="selectedRowType=='Table'" class="row">
          <div class="col-xs-12">
            <h3 style="padding-top:10px">Settings for table {{selectedTable}}</h3>
          </div>      
        </div>
        <hr style="border-width:1.5px ;color:black;">
    </div>

    <div class="container-fluid">

        <div v-if="selectedRowType=='Table'" class="row">
          <table-gen-form
          :number-of-rows = "numberOfTableRows"
          :key="reRenderKey"
          @update-table-settings="updateTableSettings" 
          >
          </table-gen-form>
        </div>


        <div v-if= "getColumnRootDataType() =='String' && selectedRowType!='Table' " class="string-data-gen-settings"> 
            <string-gen-form
              :generator-settings="selectedColumnDataGenSettings"
              :key="reRenderKey"
              :is-nullable="this.selectedColumnProperties['is_nullable']"
              :character-maximum-length="this.selectedColumnProperties['character_maximum_length']"
              @update-column-gen-settings="updateColumnGenSettings" 
            >
            </string-gen-form>
        </div>
        <div v-if= "getColumnRootDataType() =='Date'  && selectedRowType!='Table'" class="string-data-gen-settings"> 
            <date-gen-form
              :generator-settings="selectedColumnDataGenSettings"
              :is-nullable="this.selectedColumnProperties['is_nullable']"
              :key="reRenderKey"
              @update-column-gen-settings="updateColumnGenSettings" 
            >
            </date-gen-form>
        </div>
        <div v-if= "getColumnRootDataType() =='Datetime'  && selectedRowType!='Table'" class="string-data-gen-settings"> 
            <datetime-gen-form
              :generator-settings="selectedColumnDataGenSettings"
              :is-nullable="this.selectedColumnProperties['is_nullable']"
              :key="reRenderKey"
              @update-column-gen-settings="updateColumnGenSettings" 
              >
            </datetime-gen-form>
        </div>
        <div v-if= "getColumnRootDataType() =='Integer'  && selectedRowType!='Table'" class="string-data-gen-settings"> 
            <number-gen-form
              :generator-settings="selectedColumnDataGenSettings"
              :number-of-rows = "numberOfTableRows"
              :is-primary-key="this.selectedColumnProperties['is_primary_key']"
              :is-foreign-key="this.selectedColumnProperties['is_foreign_key']"
              :is-nullable="this.selectedColumnProperties['is_nullable']"
              :key="reRenderKey"
              @update-column-gen-settings="updateColumnGenSettings" 
              >
            </number-gen-form>
        </div>
        </div>
    `      
}