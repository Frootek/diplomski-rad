//import { createApp } from 'vue'
var Vue  = require ('vue')

import tablesAndColumns from './tablesAndColumns.js'
import dataGenSettings from './dataGenSettings.js'
import dataPreview  from './dataPreview.js'
import  * as helper from './helper.js'

//const app = createApp({
const app = Vue.createApp({
components: {
    tablesAndColumns,
    dataGenSettings,
    dataPreview
},
data() {
    return {
      table_name : 'owner',
      selected_row_type : null,
      selected_table : null,
      selected_column : null,
      selected_column_properties:null,
      selected_column_data_gen_settings:null,
      pgMetadata : null,
      re_render_key : 0
    }
  },
mounted() {
    helper.getTablesAndColumns().then((result) => {this.pgMetadata = result} )
},
computed: {

    numberOfRows(){
        if(this.selected_table != null) {
            return this.pgMetadata[this.selected_table]['number_of_rows']
        }
        return null
    },
    tableColumns() {
        if(this.selected_table!= null)
        {
            return Object.keys(this.pgMetadata[this.selected_table]['columns'])
        }
        return null;
    },
    dataPreviewGeneratedData() {
        if(this.selected_table != null) {
            let generatedData = {}
            let table = this.pgMetadata[this.selected_table]
            let columns = table['columns']
            let keys = Object.keys(columns)
    
            for (const key in keys) {
                generatedData[keys[key]] = helper.generateDataForColumn(columns[keys[key]]['generator_settings'],100,table['table_name'],keys[key],this.pgMetadata )
            }
            return generatedData
        }
        return null;
    },
},
methods : {
    async testGetMetadata(event) {
        let result = await helper.getTablesAndColumns()
        console.log('a')
        console.log(result)
    },
    updateTableColumnGenSettings(table_name, column_name, gen_settings) {
        this.pgMetadata[table_name]['columns'][column_name]['generator_settings'] = gen_settings
    },
    updateTableSettings(number_of_rows) {
        this.pgMetadata[this.selected_table]['number_of_rows'] = number_of_rows
        //update primary key

    },
    noRowSelected() {
        this.re_render_key += 1
        this.selected_row_type = null
        this.selected_table = null
        this.selected_column = null
    },
    tableRowSelected(tableName) {
        this.re_render_key += 1
        this.selected_row_type = 'Table'
        this.selected_table = tableName
        this.selected_column = null
    },
    columnRowSelected(tableName, columnName) {
        this.re_render_key += 1
        this.selected_row_type = 'Column'
        this.selected_table = tableName
        this.selected_column = columnName
        this.selected_column_properties = this.pgMetadata[this.selected_table]['columns'][this.selected_column]
        this.selected_column_data_gen_settings = this.pgMetadata[this.selected_table]['columns'][this.selected_column]['generator_settings']    },
},
template: `
    <div class="container-fluid">
    <div class="row">
    <!-- <button type="button" class="btn btn-secondary" @click="testGetMetadata($event)">Ajde testiraj se</button> -->
    <div class="col-sm-8">
        <div id="tables" class="container-fluid"  style="border:1px solid #000000; padding-left:10px; height:57.5vh;">
            <tables-and-columns
                :tables-and-columns = "pgMetadata"
                @no-selected-row="noRowSelected" 
                @selected-table-row="tableRowSelected" 
                @selected-column-row="columnRowSelected">
            </tables-and-columns>
        </div>
    </div>

    <div class="col-sm-4">
        <div id="settings"  class="container-fluid" style="border:1px solid #000000; height:57.5vh; padding-left:10px">                
            <data-gen-settings
            :selected-column-data-gen-settings ="selected_column_data_gen_settings"
            :selected-column-properties ="selected_column_properties"
            :selected-row-type="selected_row_type"
            :selected-table="selected_table"
            :number-of-table-rows= "numberOfRows"
            :selected-column="selected_column"
            :re-render-key = "re_render_key"
            @update-table-column-gen-settings="updateTableColumnGenSettings"
            @update-table-settings = "updateTableSettings"
            >
            </data-gen-settings>
        </div>
    </div>

    </div>

    <div class="row">
        <div class="col-sm-12" style="padding-top:25px;">
            <div id="table preview"  style="border:1px solid #000000;padding-left:10px; height:27vh">
                <data-preview
                :selected-table = "selected_table"
                :table-columns = "tableColumns"
                :selected-column = "selected_column"
                :generated-data = "dataPreviewGeneratedData"
                :number-of-rows = 100
                >
                </data-preview>

            </div>
        </div>
    </div>
    </div>
` 
})
app.mount('#mainComponent')
