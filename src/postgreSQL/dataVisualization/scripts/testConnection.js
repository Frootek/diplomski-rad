
//const db = require("db")
const {Client} = require("pg")

const client = new Client({
    user: "musicbrainz",
    host: "localhost",
    database: "musicbrainz",
    password: "musicbrainz",
    port: 15432,
})

const client2 = new Client({
    user: "postgres",
    host: "localhost",
    database: "studAdmin",
    password: "Ovo je password",
    port: 5432,
})



async function main(){
    client2.connect().then(() => console.log("Connection successful")).catch(e => console.log("err"))

    var queryRelations = {
        text : `
        SELECT relname as relation, c.reltuples::bigint AS estimate
        FROM   pg_class c
        JOIN   pg_namespace n ON n.oid = c.relnamespace
        WHERE nspname NOT IN ('pg_catalog', 'information_schema')
        and  relname LIKE '%dent'
        AND C.relkind <> 'i'
        AND nspname !~ '^pg_toast';`
    }

    table_size = await client2.query(queryRelations).then(res=> {
        return res.rows
    })


    console.log(table_size)

}
main()