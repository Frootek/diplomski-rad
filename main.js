const { app, BrowserWindow, Notification} = require('electron')
const { dialog, remote } = require('electron')
const fs = require('fs');

const ipc = require("electron").ipcMain


var win;

function createWindow () {
  // Create the browser window.
   win = new BrowserWindow({
    width: 1250,
    height: 600,
    webPreferences: {
      enableRemoteModule : true,
      nodeIntegration: true
    }
  })

  win.setMenu(null)

  // and load the index.html of the app.
  win.loadFile('src/index.html')

}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(createWindow)


// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    //await session.defaultSession.clearStorageData();
    app.quit()
  }
})

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.

  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow()
  }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.


//communication beetwen processes

ipc.on("connection-success",function (event, client,password) {
  win.webContents.send("connection-success-notify",client,password)

})

ipc.on("connection-success-mysql",function (event, client,password) {
  win.webContents.send("connection-success-notify-mysql",client,password)

})

ipc.on("select-directory",function (event, metadata) {
  dir = dialog.showOpenDialog({
    properties: ['openDirectory'] 
  }).then(result => {
    //console.log('ovo ti je pgmetadata' + JSON.stringify(metadata))
    try {
       fs.writeFileSync(result['filePaths'] + '/dbloader_config.json', JSON.stringify(metadata), 'utf-8');
       const notification = {
          title: 'Success.',
          body: 'Config file exported to: ' + result['filePaths'] + '/dbloader_config.json'  ,
        }
      new Notification(notification).show()
    }
    catch(e) { 
      alert('error==>'+ JSON.stringify(e)) 
    }
  });
})


ipc.on("file-imported",function (event, filepath) {
  const notification = {
    title: 'Success.',
    body: 'Successfully imported file: ' + filepath  ,
  }
  new Notification(notification).show()
  
})